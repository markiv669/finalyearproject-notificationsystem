﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using CNS.Models;
using CNS.Models.PostData;
using System.Data;
using MySql.Data.MySqlClient;
using CNS.Models.CRUD;
using System.Collections.Generic;
using CNS.Models.PushNotification.Pushy;
using CNS.Models.CRUD.Read;

namespace CNS.Scheduler
{
    public class ScheduleTask : ScheduledProcessor
    {

        public ScheduleTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
        }

        protected override string Schedule => "* * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            System.Console.WriteLine("send notice task running");
            //write code to read tokens for event notification sending
            List<string> n_message = new List<string>();
            List<string> e_date = new List<string>();
            List<string> tokenID = new List<string>();
            List<string> notificationID = new List<string>();
            string[] facultyList = ReadFacultyList.Read();

            for (int i = 0; i < facultyList.Length; i++)
            {
                facultyList[i] = facultyList[i].ToLower();
            }

            for (int i = 0; i < facultyList.Length; i++)
            {
                string eventquery = "select * from " + facultyList[i] + "_event_participant_table";
                string notificationQuery = "select e_title,e_deadline from " + facultyList[i] + "_notification where n_id=@n_id";
                string tokenquery = "select t_id from device_token where u_id=@u_id";
                EventParticipantsData[] eventParticipantsData = null;


                MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
                int count = 0;
                using (connection)
                {
                    try
                    {
                        connection.Open();
                        MySqlCommand eventParticipantsRead = new MySqlCommand(eventquery, connection);
                        using (MySqlDataAdapter eventAdapter = new MySqlDataAdapter(eventParticipantsRead))
                        {
                            DataTable dataTable = new DataTable();
                            DataTable fileDataTable = new DataTable();
                            eventAdapter.Fill(dataTable);
                            count = dataTable.Rows.Count;
                            if (count > 0)
                            {
                                eventParticipantsData = new EventParticipantsData[count];
                                for (int j = 0; j < count; j++)
                                {
                                    eventParticipantsData[j] = new EventParticipantsData();
                                }
                                for (int index = 0; index < count; index++)
                                {
                                    eventParticipantsData[index].n_id = dataTable.Rows[index]["n_id"].ToString();
                                    eventParticipantsData[index].u_id = dataTable.Rows[index]["u_id"].ToString();
                                    notificationID.Add(eventParticipantsData[index].n_id);

                                    MySqlCommand messageCommand = new MySqlCommand(notificationQuery, connection);
                                    messageCommand.Parameters.AddWithValue("@n_id", eventParticipantsData[index].n_id);
                                    using (MySqlDataReader reader = messageCommand.ExecuteReader())
                                        while (reader.Read())
                                        {
                                            n_message.Add(reader.GetValue(0).ToString());
                                            e_date.Add(reader.GetValue(1).ToString());


                                        }
                                    //n_message.Add((string)messageCommand.ExecuteScalar());

                                    MySqlCommand tokenCommand = new MySqlCommand(tokenquery, connection);
                                    tokenCommand.Parameters.AddWithValue("@u_id", eventParticipantsData[index].u_id);
                                    tokenID.Add((String)tokenCommand.ExecuteScalar());
                                }
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    Event_SendPushNotification(n_message, notificationID, e_date, tokenID);
                }
            }
            return Task.CompletedTask;
        }


        public void Event_SendPushNotification(List<string> n_message, List<string> notificationID, List<string> e_date, List<string> deviceTokens)
        {
            // Prepare array of target device tokens
            // List<string> deviceTokens = new List<string>();

            // Add your device tokens here
            // deviceTokens.Add("cdd92f4ce847efa5c7f");

            // Convert to string[] array
            //string[] to = deviceTokens.ToArray();

            // Optionally, send to a publish/subscribe topic instead

            // string to = "/topics/" + data.faculty + data.n_semester;
            // to = to.ToLower();

            try
            {
                // Set payload (it can be any object)
                for (int i = 0; i < deviceTokens.Count; i++)
                {
                    string remainingDays;
                    //  DateTime enteredDate = DateTime.Parse(e_date[i]);
                    //   DateTime enteredDate = DateTime.ParseExact(e_date[i], "dd/MM/yyyy", null);
                    DateTime enteredDate = DateTime.ParseExact(e_date[i], @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime today = DateTime.Today;

                    DateTime datevalue = (Convert.ToDateTime(enteredDate.ToString()));

                    int eventDay = datevalue.Day;
                    int eventMonth = datevalue.Month;
                    int eventYear = datevalue.Year;

                    if (today.Day > eventDay || today.Month > eventMonth || today.Year > eventYear)
                    {
                        continue;
                    }
                    else
                    {
                        int tmp = eventDay - today.Day;
                        if (tmp <= 2 && tmp > 0)
                            remainingDays = tmp.ToString() + " days remaining for event.";
                        else
                            continue;
                    }
                    var payload = new Dictionary<string, string>();

                    // Add message parameter to dictionary
                    payload.Add("message", remainingDays);
                    payload.Add("title", n_message[i]);
                    payload.Add("notificationID", notificationID[i]);

                    // iOS notification fields
                    var notification = new Dictionary<string, object>();

                    notification.Add("badge", 1);
                    notification.Add("sound", "ping.aiff");
                    notification.Add("body", "Event is coming near");
                    if (deviceTokens[i] != null)
                    {
                        // Prepare the push HTTP request
                        PushyPushRequest push = new PushyPushRequest(payload, deviceTokens[i], notification);


                        try
                        {
                            // Send the push notification
                            PushyAPI.SendPush(push);
                        }
                        catch (Exception exc)
                        {
                            // Write error to console
                            Console.WriteLine(exc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }

    public class CleanUpStorage : ScheduledProcessor
    {

        public CleanUpStorage(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
        }


        protected override string Schedule => "* * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            System.Console.WriteLine("cleanup task running");
            return Task.CompletedTask;
        }
    }
}
