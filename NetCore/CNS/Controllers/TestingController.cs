using System.Collections.Generic;
using CNS.Models.PostData;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CNS.Controllers
{
    [Route("[controller]/[action]")]
    
    public class TestingController : Controller
    {
        [HttpGet]
        public JsonResult Get()
        {
            StatusModel status=new StatusModel();
            status.code = 0;
            status.message = "failed to read notification. Database error";
            return Json(status);
           
        }

        [HttpPost]
        public IEnumerable<string> Post([FromBody] string value)
    {
        return new string[] { "value1", "value2" };
    } 
    }
}