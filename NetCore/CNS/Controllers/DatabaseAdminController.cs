
using CNS.Models.CRUD;
using Microsoft.AspNetCore.Mvc;
using CNS.Models.PostData;
using CNS.Models.FileHandeling;
using System.Text;
using System;
using System.Net.Mail;
using CNS.Models.SendEmail;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using CNS.Models.CRUD.Insert;
using CNS.Models.CRUD.Delete;

namespace CNS.Controllers
{
    [Route("[controller]/[action]")]
    public class DatabaseAdminController : Controller
    {
        [HttpPost]
        public async Task<string> UserSignUp([FromForm] UserSignupData data)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                if (UserAuthorityVerification.ToAlterUser(data))
                {
                    PasswordGenerator PasswordGenerator = new PasswordGenerator();
                    string randomPassword = PasswordGenerator.CreatePassword(8);

                    if (InsertNewUser.Insert(data, randomPassword))
                    {

                        if (SendMail.Send(data.u_id, data.primary_email, randomPassword))
                        {
                            //StoreFile.SaveUserProfileImage(data);
                            try
                            {
                                var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.profilePicturePath, data.u_id + "." + StoreFile.GetFileExtension(data.image.FileName));
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    await data.image.CopyToAsync(stream);
                                }
                                status.code = 1;
                                status.message = "user added to database";
                            }
                            catch (System.Exception ex)
                            {
                                System.Console.WriteLine(ex.Message);
                                status.code = 0;
                                status.message = "storing user image failed";
                            }

                            return (JsonConvert.SerializeObject(status));
                        }
                        else
                        {
                            //Roll Back Changes
                            if (DeleteUser.Delete(data.u_id))
                            {
                                status.code = 0;
                                status.message = "failed to send mail. users signup failed";
                                return (JsonConvert.SerializeObject(status));
                            }
                            else
                            {
                                status.code = 0;
                                status.message = "failed to send mail plus failed to delete user from database";
                                return (JsonConvert.SerializeObject(status));
                            }
                        }
                    }
                    else
                    {
                        status.code = 0;
                        status.message = "failed. make sure user doesnot exist";
                        return (JsonConvert.SerializeObject(status));
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized to perform database changes";
                    return (JsonConvert.SerializeObject(status));
                }
            }
            else
            {



                status.code = 0;
                status.message = "Invalid input data";

                return (JsonConvert.SerializeObject(status));

            }
        }


        [HttpPost]
        public async Task<string> EditUser([FromForm] UserSignupData data)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                if (UserAuthorityVerification.ToAlterUser(data))
                {
                    if (UpdateUserInfo.Update(data))
                    {
                        //StoreFile.SaveUserProfileImage(data);
                        try
                        {
                            var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.profilePicturePath, data.u_id + "." + StoreFile.GetFileExtension(data.image.FileName));
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                await data.image.CopyToAsync(stream);
                            }
                            status.code = 1;
                            status.message = "user data updated";
                        }
                        catch (System.Exception ex)
                        {
                            System.Console.WriteLine(ex.Message);
                            status.code = 0;
                            status.message = "storing user image failed";
                        }

                        return (JsonConvert.SerializeObject(status));

                    }
                    else
                    {
                        status.code = 0;
                        status.message = "failed to signup data. database error";
                        return (JsonConvert.SerializeObject(status));
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized to perform database changes";
                    return (JsonConvert.SerializeObject(status));
                }
            }
            else
            {

                status.code = 0;
                status.message = "Invalid input data";
                return (JsonConvert.SerializeObject(status));
            }
        }


        [HttpPost]
        public IActionResult UserDelete([FromForm]UserSignupData data)
        {
            StatusModel status = new StatusModel();
            if (data.u_id == null || data.admin_id == null || data.admin_password == null)
            {
                status.code = 0;
                status.message = "invalid input";
                return (Json(status));
            }
            if (UserAuthorityVerification.ToAlterUser(data))
            {
                if (DeleteUser.Delete(data.u_id))
                {
                    status.code = 1;
                    status.message = "user deleted sucessfully";
                    return (Json(status));
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to delete user";
                    return (Json(status));
                }
            }
            else
            {
                status.code = 0;
                status.message = "failed. not authorized to delete";
                return (Json(status));
            }
        }

        [HttpPost]
        public IActionResult SearchUser([FromForm] UserSearchModel data)
        {
            StatusModel status = new StatusModel();
            if (data.ValidateData())
            {
                data.GenerateQuery();
                UserSignupData[] result = CNS.Models.CRUD.Read.SearchUser.Search(data);
                if (result != null)
                {
                    return Json(result);
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to read users. Database error";
                    return Json(status);
                    //return (JsonConvert.SerializeObject(status));
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input data. please insert at least on field";
                return Json(status);
            }

        }


        [HttpPost]
        public IActionResult AddFaculty([FromForm] AddFacultyModel faculty)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                UserSignupData data = new UserSignupData();
                data.admin_id = faculty.admin_id;
                data.admin_password = faculty.admin_password;
                if (UserAuthorityVerification.ToAlterUser(data))
                {
                    if (AddNewFaculty.Add(faculty))
                    {
                        status.code = 1;
                        var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, faculty.faculty_name.ToUpper());
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        status.message = "faculty sucessfully added";
                    }
                    else
                    {
                        status.code = 0;
                        status.message = "Error storing faculty. Database error";
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized";
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid data";
            }

            return Json(status);
        }

        [HttpPost]
        public IActionResult RemoveFaculty([FromForm] AddFacultyModel faculty)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                UserSignupData data = new UserSignupData();
                data.admin_id = faculty.admin_id;
                data.admin_password = faculty.admin_password;
                if (UserAuthorityVerification.ToAlterUser(data))
                {
                    if (AddNewFaculty.Remove(faculty.faculty_name))
                    {
                        var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, faculty.faculty_name.ToUpper());
                        if (!Directory.Exists(path))
                        {

                        }
                        else
                        {
                            Directory.Delete(path, true);
                        }
                        status.code = 1;
                        status.message = "faculty sucessfully removed";
                    }
                    else
                    {
                        var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, faculty.faculty_name.ToUpper());
                        if (!Directory.Exists(path))
                        {

                        }
                        else
                        {
                            Directory.Delete(path, true);
                        }
                        status.code = 0;
                        status.message = "There was some error removing faculty. Database error. all faculty data has been deleted";
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized";
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid data";
            }

            return Json(status);
        }

        [HttpPost]
        public IActionResult AddBatch([FromForm] AddBatchData data)
        {
            StatusModel status = new StatusModel();
            UserSignupData userSignUpData = new UserSignupData();
            userSignUpData.admin_password = data.password;
            userSignUpData.admin_id = data.admin_id;
            if (UserAuthorityVerification.ToAlterUser(userSignUpData))
            {
                if (InsertNewBatch.Insert(data))
                {
                    status.code = 1;
                    status.message = "sucessfully added batch";
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to add batch. database error";
                }
            }
            else
            {
                status.code = 0;
                status.message = "not authorized";
            }
            return Json(status);
        }

        public IActionResult RemoveBatch([FromForm] AddBatchData data)
        {
            StatusModel status = new StatusModel();
            UserSignupData userSignUpData = new UserSignupData();
            userSignUpData.admin_password = data.password;
            userSignUpData.admin_id = data.admin_id;
            if (UserAuthorityVerification.ToAlterUser(userSignUpData))
            {
                if (DeleteBatch.Delete(data))
                {
                    status.code = 1;
                    status.message = "sucessfully deleted batch";
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to delete batch. database error";
                }
            }
            else
            {
                status.code = 0;
                status.message = "not authorized";
            }
            return Json(status);
        }

        public IActionResult EditFaculty([FromForm] AddFacultyModel faculty)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                UserSignupData data = new UserSignupData();
                data.admin_id = faculty.admin_id;
                data.admin_password = faculty.admin_password;
                if (UserAuthorityVerification.ToAlterUser(data))
                {
                    if (AddNewFaculty.Edit(faculty))
                    {
                        status.code = 1;
                        status.message = "faculty information sucessfully updated";
                    }
                    else
                    {
                        status.code = 0;
                        status.message = "Error storing faculty. Database error";
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized";
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input data";
            }
            return Json(status);
        }
    }

    public class PasswordGenerator
    {
        public string CreatePassword(int length)
        {
            const string valid = "ab!cd@efgh$0ij%kl^mn&o9p*qr(8stuvwx6yzABCD5E)7FGHIJKLMNOPQRST#UVWXY@Z1234";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }

}