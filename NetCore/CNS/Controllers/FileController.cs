using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CNS.Models.FileHandeling;
using CNS.Models.CRUD.Read;

namespace CNS.Controllers
{
    [Route("[controller]/[action]")]
    public class FileController : Controller
    {
        [HttpGet]
        public IActionResult ProfilePicture(string name)
        {
            try
            {
                var file = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.profilePicturePath, name);
                string mimetype = MIMEAssistant.GetType(StoreFile.GetFileExtension(name));
               
                return PhysicalFile(file, mimetype);

            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return Json("0");
            }

        }

        [HttpGet]

        public IActionResult NotificationImage(string faculty, string name)
        {
            try
            {
                string mimetype = "image/jpg";
                mimetype = MIMEAssistant.GetType(StoreFile.GetFileExtension(name));
                mimetype = MIMEAssistant.GetType(StoreFile.GetFileExtension(name));
                var file = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath,
                                        faculty.ToUpper(), name);
                string fileName = ReadFileName.Read(name, faculty);
                if (fileName == null)
                {
                    fileName = name;
                }
               
                    return PhysicalFile(file, mimetype, fileName);
                
              
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return Json("0");
            }
        }

        [HttpGet]
        public IActionResult NotificationFile(string faculty, string name)
        {
            try
            {
                string mimetype = "text/txt";
                mimetype = MIMEAssistant.GetType(StoreFile.GetFileExtension(name));
                var file = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath,
                                        faculty.ToUpper(), name);
                string fileName = ReadFileName.Read(name, faculty);
                if (fileName == "")
                {
                    fileName = name;
                }
              
                    return PhysicalFile(file, mimetype, fileName);
               
                  
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return Json("0");
            }
        }
    }
}