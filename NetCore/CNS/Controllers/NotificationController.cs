using Microsoft.AspNetCore.Mvc;
using CNS.Models.PostData;
using CNS.Models.CRUD;
using CNS.Models.FileHandeling;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Net.Http;
using CNS.Models.PushNotification.Pushy;
using CNS.Models.PushNotification;
using CNS.Models.CRUD.Update;
using CNS.Models.CRUD.Delete;
using CNS.Models.CRUD.Read;

namespace CNS.Controllers
{
    [Route("[controller]/[action]")]
    public class NotificationController : Controller
    {
        [HttpPost]
        [RequestSizeLimit(200_000_000_0)]
        public async Task<IActionResult> sendNotification([FromForm] NotificationData data)
        {
            System.Console.WriteLine(data.n_message);
            StatusModel status = new StatusModel();
            // if (ModelState.IsValid && data.EventTypeValidate())
            if (true)
            {
                if (UserAuthorityVerification.ToAlterNotification(data))
                {
                    data.ComputeDateTime();
                    data.CountFileType();
                    if (InsertNewNotification.Insert(data))
                    {
                        if (data.file != null)
                        {
                            if (InsertFileData.Insert(data))
                            {
                                try
                                {
                                    for (int i = 0; i < data.file.Count; i++)
                                    {
                                        var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, data.faculty, data.n_id + i + "$" + data.file[i].FileName.ToString());
                                        using (var stream = new FileStream(path, FileMode.Create))
                                        {
                                            await data.file[i].CopyToAsync(stream);
                                        }
                                    }
                                    status.code = 1;
                                    status.message = "notification uploaded sucessfully";
                                }
                                catch (System.Exception ex)
                                {
                                    System.Console.WriteLine(ex.Message);
                                    status.code = 0;
                                    status.message = "storing files failed";
                                }
                            }
                            else
                            {
                                // code to ROllBack notification upload changess
                                if (CNS.Models.CRUD.DeleteNotification.Delete(data))
                                {
                                    status.code = 0;
                                    status.message = "notification upload failed because saving file info in database failed ";

                                }
                                else
                                {
                                    status.code = 1;
                                    status.message = "server error. notification uploaded without files";
                                }
                            }

                        }
                        else
                        {
                            status.code = 1;
                            status.message = "notification uploaded sucessfully";
                            //return (JsonConvert.SerializeObject(status));
                        }
                    }
                    else
                    {
                        status.code = 0;
                        status.message = "notification upload failed";
                        // return (JsonConvert.SerializeObject(status));
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized";
                    //return (JsonConvert.SerializeObject(status));
                }
            }
            else
            {
                //  status.code = 0;
                //  status.message = "invalid input data";
            }
            if (status.code == 1)
            {
                PushNotification.NewNotice_SendPushNotification(data);
                //SEND PUSH NOTIFICATION
            }
            return Json(status);
        }



        [HttpPost]
        [RequestSizeLimit(200_000_000_0)]
        public async Task<IActionResult> EditNotification([FromForm] EditNotificationData data)
        {
            System.Console.WriteLine(data.n_message);
            StatusModel status = new StatusModel();

            int currentFileCount = data.file_count;
            int currentImageCount = data.image_count;
            int currentTotalFileCount = (currentFileCount + currentImageCount) + 1;
            if (ModelState.IsValid && data.EventTypeValidate())
            {
                if (UserAuthorityVerification.ToEditNotification(data))
                {
                    data.CountFileType();
                    try
                    {
                        for (int i = 0; i < data.file.Count; i++)
                        {
                            var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, data.faculty, data.n_id + (i + currentTotalFileCount) + "$" + data.file[i].FileName.ToString());
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                await data.file[i].CopyToAsync(stream);
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                        status.code = 0;
                        status.message = "storing files failed. notification not updated";
                        return Json(status);
                    }

                    if (UpdateNotification.Edit(data))
                    {
                        //1*delete f_id from delete file list
                        if (DeleteFileID.Delete(data))
                        {

                        }

                        for (int j = 0; j < data.delete_file_name.Count; j++)
                        {
                            var path = Path.Combine(Directory.GetCurrentDirectory(), StoreFile.filePath, data.faculty, data.delete_file_name[j].ToString());
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                            }
                        }

                        // * */1//
                        if (data.file != null)
                        {
                            if (InsertFileData.EditInsert(data, currentTotalFileCount))
                            {
                                status.code = 1;
                                status.message = "notification uploaded sucessfully";
                            }
                            else
                            {
                                // code to ROllBack notification upload changess
                                status.code = 1;
                                status.message = "server error. notification uploaded without files";

                            }

                        }

                    }
                    else
                    {
                        status.code = 0;
                        status.message = "notification upload failed";
                        // return (JsonConvert.SerializeObject(status));
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "not authorized";
                    //return (JsonConvert.SerializeObject(status));
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input data";
            }
            if (status.code == 1)
            {
                PushNotification.EditNotice_SendPushNotification(data);
                //SEND PUSH NOTIFICATION
            }
            return Json(status);
        }



        [HttpPost]
        public IActionResult GetNotification([FromForm] NotificationReadData data)
        {

            GetNotificationResponse notificationData = NotificationRead.ReadNotification(data);
            if (notificationData != null)
            {
                return Json(notificationData);
                //return (JsonConvert.SerializeObject(notificationData));
            }
            else
            {
                if (notificationData == null)
                {
                    notificationData = new GetNotificationResponse();
                }
                notificationData.code = 0;
                notificationData.message = "failed to read notification. Database error";
                return Json(notificationData);
                //return (JsonConvert.SerializeObject(status));
            }
        }

        [HttpPost]
        public IActionResult GetSingleNotification([FromForm] NotificationReadData data)
        {
            StatusModel[] status = new StatusModel[1];
            status[0] = new StatusModel();
            NotificationData[] notificationData = NotificationRead.ReadSingleNotification(data);
            if (notificationData != null)
            {
                return Json(notificationData);
            }
            else
            {
                status[0].code = 0;
                status[0].message = "failed to read notification. Database error";
                return Json(status);
                //return (JsonConvert.SerializeObject(status));
            }
        }

        [HttpGet]
        public IActionResult GetEventParticipants(string u_faculty, string n_id)
        {
            StatusModel status = new StatusModel();
            UserSignupData[] fetchedData = ReadEventParticipantsList.Read(u_faculty, n_id);
            if (fetchedData != null)
            {
                return Json(fetchedData);
            }
            else
            {
                status.code = 0;
                status.message = "no data";
                return Json(status);
            }

        }

        [HttpPost]
        public IActionResult DeleteNotification([FromForm] NotificationData data)
        {
            StatusModel status = new StatusModel();
            if (data.DeleteNotificationValidate())
            {
                if (UserAuthorityVerification.ToAlterNotification(data))
                {
                    if (CNS.Models.CRUD.DeleteNotification.Delete(data))
                    {
                        status.code = 1;
                        status.message = "notification deleted sucessfully";
                        return Json(status);
                    }
                    else
                    {
                        status.code = 0;
                        status.message = "unable to delete. database error";
                        return Json(status);
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "unable to delete. not authorized";
                    return Json(status);
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input";
                return Json(status);
            }
        }


        public void SendPushNotification(NotificationData data)
        {
            // Prepare array of target device tokens
            // List<string> deviceTokens = new List<string>();

            // Add your device tokens here
            // deviceTokens.Add("cdd92f4ce847efa5c7f");

            // Convert to string[] array
            // string[] to = deviceTokens.toArray();

            // Optionally, send to a publish/subscribe topic instead

            string to = "/topics/" + data.faculty + data.n_semester;
            to = to.ToLower();


            // Set payload (it can be any object)
            var payload = new Dictionary<string, string>();

            // Add message parameter to dictionary
            payload.Add("message", data.n_message);
            payload.Add("title", data.n_title);
            payload.Add("notificationID", data.n_id);
            // iOS notification fields
            var notification = new Dictionary<string, object>();

            notification.Add("badge", 1);
            notification.Add("sound", "ping.aiff");
            notification.Add("body", data.n_message);

            // Prepare the push HTTP request
            PushyPushRequest push = new PushyPushRequest(payload, to, notification);
            System.Console.WriteLine(to);

            try
            {
                // Send the push notification
                PushyAPI.SendPush(push);
            }
            catch (Exception exc)
            {
                // Write error to console
                Console.WriteLine(exc);
            }
        }
    }
}
