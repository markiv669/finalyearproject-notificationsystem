using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using CNS.Models.PostData;
using CNS.Models.CRUD;
using CNS.Models.SendEmail;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using CNS.Models.PushNotification;
using CNS.Models.CRUD.Read;

namespace CNS.Controllers
{
    [Route("[controller]/[action]")]
    public class UserActivityController : Controller
    {
        //Checks for username and password and return all the users data if login credentials match
        [HttpPost]
        public IActionResult Login([FromForm] UserLoginData user)
        {
            if (ModelState.IsValid)
            {
                UserSignupData result = LoginCredentialsRead.LoginCheck(user);
                return Json(result);
            }
            else
            {
                StatusModel status = new StatusModel();
                status.code = 0;
                status.message = "invalid input data";
                return Json(status);
            }
        }

        [HttpPost]
        public JsonResult ChangeCredential([FromForm] CredentialsChangeData data)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                if (UpdateCredentials.UpdateUserCredentials(data))
                {
                    status.code = 1;
                    status.message = "sucessfully changed password";
                    return Json(status);
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to change password. Re-check your current password";
                    return Json(status);
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input data";
                return Json(status);
            }
        }

        [HttpPost]
        public JsonResult ForgotPassword([FromForm] ForgotPasswordData data)
        {
            StatusModel status = new StatusModel();
            if (ModelState.IsValid)
            {
                PasswordGenerator passwordGenerator = new PasswordGenerator();
                string randomPassword = passwordGenerator.CreatePassword(8);

                if (InsertForgotPassword.Insert(data, randomPassword))
                {
                    if (SendMail.Send(data.u_id, data.email, randomPassword))
                    {
                        status.code = 0;
                        status.message = "check your email for new password";
                        return Json(status);
                    }
                    else
                    {
                        //Roll Back changes
                        DeleteForgotPassword.Delete(data.u_id);
                        status.code = 0;
                        status.message = "failed to send email. check your internet connection";
                        return Json(status);
                    }
                }
                else
                {
                    status.code = 0;
                    status.message = "there was an error. please check your u_id or email";
                    return Json(status);
                }
            }
            else
            {
                status.code = 0;
                status.message = "invalid input data";
                return Json(status);
            }
        }

        [HttpPost]
        public JsonResult RegisterDeviceToken([FromBody] FcmToken token)
        {
            StatusModel status = new StatusModel();
            if (token.u_id != null)
            {
                System.Console.WriteLine(token.tokenID);
                System.Console.WriteLine(token.u_id);
                if (InsertDeviceToken.Insert(token))
                {
                    status.code = 1;
                    status.message = "device registered sucessfully";
                }
                else
                {
                    status.code = 0;
                    status.message = "failed to register device in database";
                }
            }
            return Json(status);
        }


        [HttpPost]
        public IActionResult InsertQuery(QueryData data)
        {
            StatusModel status = new StatusModel();
            if (CNS.Models.CRUD.Insert.InsertQuery.Insert(data))
            {
                status.code = 1;
                status.message = "your query has been posted";
                PushNotification.Query_SendPushNotification(data.faculty, data.n_id, data.q_message);
            }
            else
            {
                status.code = 0;
                status.message = "Error posting query";
            }

            return Json(status);
        }


        [HttpPost]
        public IActionResult ReadQuery(QueryReadData data)
        {
            StatusModel status = new StatusModel();
            QueryData[] queryData = CNS.Models.CRUD.Read.ReadQuery.Read(data);
            if (queryData != null)
            {
                return Json(queryData);
            }
            else
            {
                status.code = 0;
                status.message = "Error reading query";
                return Json(status);
            }
        }

        [HttpPost]
        public IActionResult DeleteQuery(QueryData data)
        {
            StatusModel status = new StatusModel();
            if (CNS.Models.CRUD.Delete.DeleteQuery.Delete(data))
            {
                status.code = 1;
                status.message = "sucessfully deleted";
            }
            else
            {
                status.code = 0;
                status.message = "Error deleting. try again later";
            }


            return Json(status);
        }

        [HttpPost]
        public IActionResult EditQuery(QueryData data)
        {
            StatusModel status = new StatusModel();
            if (CNS.Models.CRUD.UpdateQuery.Update(data))
            {
                status.code = 1;
                status.message = "sucessfully updated";
                PushNotification.Query_SendPushNotification(data.faculty, data.n_id, "Edited: " + data.q_message);
            }
            else
            {
                status.code = 0;
                status.message = "Error editing. try again later";
            }
            return Json(status);
        }

        [HttpPost]
        public IActionResult ParticipateToEvent(EventParticipantsData data)
        {
            StatusModel status = new StatusModel();
            if (CNS.Models.CRUD.Insert.ParticipateToEvent.Insert(data))
            {
                status.code = 1;
                status.message = "Sucessfully added to event";
            }
            else
            {
                status.code = 0;
                status.message = "Error. try again later";
            }
            return Json(status);
        }

        [HttpPost]
        public IActionResult UnparticipateFromEvent(EventParticipantsData data)
        {
            StatusModel status = new StatusModel();
            if (CNS.Models.CRUD.Delete.UnparticipateFromEvent.Delete(data))
            {
                status.code = 1;
                status.message = "Sucessfully removed from event";
            }
            else
            {
                status.code = 0;
                status.message = "Error. try again later";
            }
            return Json(status);
        }

        [HttpPost]
        public IActionResult ReadEvent(EventParticipantsData data)
        {
            StatusModel status = new StatusModel();
            NotificationData[] notificationData = CNS.Models.CRUD.Read.ReadEvent.Read(data);
            if (notificationData != null)
            {

                return Json(notificationData);
            }
            else
            {
                status.code = 0;
                status.message = "no Events";
                return Json(status);
            }
        }

        public IActionResult GetFacultyList()
        {
            string[] facultyList = ReadFacultyList.Read();
            return Json(facultyList);
        }

        [HttpPost]
        public IActionResult GetBatchList(string faculty)
        {
            if (faculty != null)
            {
                string[] batchList = ReadBatchList.Read(faculty);
                return Json(batchList);
            }
            else
            {
                return Json("'code':'0'");
            }
        }

        [HttpGet]
        public IActionResult GetCollegeInfo()
        {
            StatusModel status = new StatusModel();
            FacultyData[] CollegeInfoData = ReadCollegeInfo.Read();
            if (CollegeInfoData != null)
            {
                return Json(CollegeInfoData);
            }
            else
            {
                status.code = 0;
                status.message = "no information to fetch";
                return Json(status);
            }
        }

        // [HttpGet]
        // public IActionResult GetCollegeInfo()
        // {
        //     StatusModel status = new StatusModel();
        //     FacultyData[] CollegeInfoData = ReadCollegeInfo.Read();
        //     GetFacultyResponse response = new GetFacultyResponse();
        //     if (CollegeInfoData != null)
        //     {
        //         response.faculty = CollegeInfoData;
        //         response.code = 1;
        //         response.message = "sucess";
        //         return Json(response);
        //     }
        //     else
        //     {
        //         response.code = 0;
        //         response.message = "no information to fetch";
        //         return Json(response);
        //     }
        // }

    }
}
