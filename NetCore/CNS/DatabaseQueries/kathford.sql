-- MySQL dump 10.16  Distrib 10.2.15-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: kathford
-- ------------------------------------------------------
-- Server version	10.2.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bsccsit_activebatch`
--

DROP TABLE IF EXISTS `bsccsit_activebatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsccsit_activebatch` (
  `batch_id` int(11) NOT NULL,
  PRIMARY KEY (`batch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsccsit_activebatch`
--

LOCK TABLES `bsccsit_activebatch` WRITE;
/*!40000 ALTER TABLE `bsccsit_activebatch` DISABLE KEYS */;
INSERT INTO `bsccsit_activebatch` VALUES (2071),(2072),(2073);
/*!40000 ALTER TABLE `bsccsit_activebatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsccsit_event_participant_table`
--

DROP TABLE IF EXISTS `bsccsit_event_participant_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsccsit_event_participant_table` (
  `n_id` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_id` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `n_id` (`n_id`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `bsccsit_event_participant_table_ibfk_1` FOREIGN KEY (`n_id`) REFERENCES `bsccsit_notification` (`n_id`) ON DELETE CASCADE,
  CONSTRAINT `bsccsit_event_participant_table_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsccsit_event_participant_table`
--

LOCK TABLES `bsccsit_event_participant_table` WRITE;
/*!40000 ALTER TABLE `bsccsit_event_participant_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `bsccsit_event_participant_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsccsit_file_table`
--

DROP TABLE IF EXISTS `bsccsit_file_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsccsit_file_table` (
  `f_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `n_id` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`f_id`),
  KEY `n_id` (`n_id`),
  CONSTRAINT `bsccsit_file_table_ibfk_1` FOREIGN KEY (`n_id`) REFERENCES `bsccsit_notification` (`n_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsccsit_file_table`
--

LOCK TABLES `bsccsit_file_table` WRITE;
/*!40000 ALTER TABLE `bsccsit_file_table` DISABLE KEYS */;
INSERT INTO `bsccsit_file_table` VALUES ('2018-09-02-19-42-200$2.jpg','2018-09-02-19-42-20','2.jpg'),('2018-09-02-19-42-201$6983423-candle-snowfall.jpg','2018-09-02-19-42-20','6983423-candle-snowfall.jpg'),('2018-09-02-19-42-202$ABSTRACT.docx','2018-09-02-19-42-20','abstract.docx'),('2018-09-02-19-42-203$bg.jpg','2018-09-02-19-42-20','bg.jpg'),('2018-09-02-19-42-204$flowers-2058090_960_720.png','2018-09-02-19-42-20','flowers-2058090_960_720.png'),('2018-09-02-19-42-205$Hawaii-Beach-Wallpaper-HD_8pA2vrZ.jpg','2018-09-02-19-42-20','hawaii-beach-wallpaper-hd_8pa2vrz.jpg'),('2018-09-02-19-42-206$test.pdf','2018-09-02-19-42-20','test.pdf'),('2018-09-02-19-42-207$water-2748640_960_720.png','2018-09-02-19-42-20','water-2748640_960_720.png');
/*!40000 ALTER TABLE `bsccsit_file_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsccsit_notification`
--

DROP TABLE IF EXISTS `bsccsit_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsccsit_notification` (
  `n_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `n_date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `n_time` time NOT NULL,
  `n_message` text COLLATE utf8_unicode_ci NOT NULL,
  `n_semester` int(11) NOT NULL,
  `n_event` int(11) NOT NULL,
  `e_location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e_deadline` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e_time` time DEFAULT NULL,
  `e_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_count` int(11) NOT NULL,
  `file_count` int(11) NOT NULL,
  PRIMARY KEY (`n_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsccsit_notification`
--

LOCK TABLES `bsccsit_notification` WRITE;
/*!40000 ALTER TABLE `bsccsit_notification` DISABLE KEYS */;
INSERT INTO `bsccsit_notification` VALUES ('2018-09-02-12-12-52','2018-09-02','12:12:52','heo\n',0,0,NULL,NULL,NULL,'Test',0,0),('2018-09-02-19-42-20','2018-09-02','19:42:20','new file name ',0,0,NULL,NULL,NULL,'file new name',6,2),('2018-09-02-20-52-32','2018-09-02','20:52:32','vikram',0,0,NULL,NULL,NULL,'Hello',0,0);
/*!40000 ALTER TABLE `bsccsit_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bsccsit_query`
--

DROP TABLE IF EXISTS `bsccsit_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bsccsit_query` (
  `n_id` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_id` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_message` text COLLATE utf8_unicode_ci NOT NULL,
  `q_date` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_time` time DEFAULT NULL,
  KEY `n_id` (`n_id`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `bsccsit_query_ibfk_1` FOREIGN KEY (`n_id`) REFERENCES `bsccsit_notification` (`n_id`) ON DELETE CASCADE,
  CONSTRAINT `bsccsit_query_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bsccsit_query`
--

LOCK TABLES `bsccsit_query` WRITE;
/*!40000 ALTER TABLE `bsccsit_query` DISABLE KEYS */;
INSERT INTO `bsccsit_query` VALUES ('2018-09-02-12-12-52','101','hello','2018-09-02','13:11:03'),('2018-09-02-19-42-20','101','there are 10 files here','2018-09-02','19:44:11'),('2018-09-02-19-42-20','071csit41','ello','2018-09-02','19:49:50');
/*!40000 ALTER TABLE `bsccsit_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_token`
--

DROP TABLE IF EXISTS `device_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_token` (
  `u_id` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `device_token_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_token`
--

LOCK TABLES `device_token` WRITE;
/*!40000 ALTER TABLE `device_token` DISABLE KEYS */;
INSERT INTO `device_token` VALUES ('071csit41','dec6ff83cdffe4c43a6147'),('101','c5c66ba36bf3fc13de8df6');
/*!40000 ALTER TABLE `device_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty` (
  `faculty_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`faculty_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty`
--

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` VALUES ('BSCCSIT'),('none');
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forgotpassword`
--

DROP TABLE IF EXISTS `forgotpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgotpassword` (
  `u_id` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newpassword` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  KEY `u_id` (`u_id`),
  CONSTRAINT `forgotpassword_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forgotpassword`
--

LOCK TABLES `forgotpassword` WRITE;
/*!40000 ALTER TABLE `forgotpassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `forgotpassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_login`
--

DROP TABLE IF EXISTS `saved_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_login` (
  `u_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `secret_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`secret_token`),
  KEY `u_id` (`u_id`),
  CONSTRAINT `saved_login_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_login`
--

LOCK TABLES `saved_login` WRITE;
/*!40000 ALTER TABLE `saved_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `u_id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `u_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `u_mname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `u_semester` int(11) NOT NULL,
  `u_faculty` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `u_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `hideword` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `father_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `father_mname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother_fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mother_mname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alternate_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordChangedStatus` int(1) DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `authority` int(1) NOT NULL,
  `u_paddress` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `u_taddress` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`u_id`),
  KEY `u_faculty` (`u_faculty`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`u_faculty`) REFERENCES `faculty` (`faculty_name`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('071csit41','vikram',NULL,'acharya',2071,'BSCCSIT','9847512536','a8de3a9ece95b09b7925d80cef2713ea','kpa',NULL,'9847586123','ina',NULL,'98475965422','kathford.cns@gmail.com',NULL,1,'071csit41.jpg',0,'ktm','ktm'),('100','dbadmin','','1',0,'none','9847012564','8b53ad0b47f1b218d221d69f2f99edef','none','','9845861258','none','','1245','kathford.cns@gmail.com','',1,'dbadmin.jpg',2,'kathmandu','kathmandu'),('101','suwas',NULL,'karki',0,'BSCCSIT','9847512536','25d55ad283aa400af464c76d713c07ad','none',NULL,'9847586123','none',NULL,'98475965422','kathford.cns@gmail.com',NULL,1,'101.jpg',1,'ktm','ktm');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-02 22:58:55
