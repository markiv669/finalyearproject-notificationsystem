using System.IO;
using System.Threading.Tasks;
using CNS.Models.PostData;
namespace CNS.Models.FileHandeling
{
    public class StoreFile
    {
        public static readonly string profilePicturePath = Path.Combine("wwwroot", "Files", "ProfilePicture");
        public static readonly string filePath = Path.Combine("wwwroot", "Files");
        public static string GetFileExtension(string filename)
        {
            string tmp = "";
            string fileExtension = "";
            int i = filename.Length - 1;
            if (i > 2)
            {
                while (i >= 0 && filename[i] != '.')
                {
                    tmp += filename[i];
                    i--;
                }
                i = tmp.Length - 1;
                while (i >= 0)
                {
                    fileExtension += tmp[i];
                    i--;
                }
            }
            return fileExtension.ToLower();
        }


    }
}