using System;
using System.Numerics;
using System.Security.Cryptography;

namespace CNS.Models.Cryptography
{

    public class RSA_LIB
    {
        public static void GenerateKeys()
        {
            using (var rsa = new RSACryptoServiceProvider(1024))
            {
                try
                {
                    string publicPrivateKeyXML = rsa.ToXmlString(true);
                    string publicOnlyKeyXML = rsa.ToXmlString(false);
                    string publickey=rsa.ToString();
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
    }
    public class RSA2
    {
        BigInteger publicKey = 0;
        BigInteger privateKey = 0;

        public BigInteger GetPublicKey()
        {
            return this.publicKey;
        }

        public BigInteger GetPrivateKey()
        {
            return this.privateKey;
        }

        public void GenerateKeys()
        {

            //others code

            KeyGenerator p = new KeyGenerator();
            KeyGenerator q = new KeyGenerator();
            KeyGenerator e = new KeyGenerator();
            KeyGenerator f = new KeyGenerator();
            KeyGenerator pub_key = new KeyGenerator();
            KeyGenerator priv_key = new KeyGenerator();
            EncryptorDecryptor cipher = new EncryptorDecryptor();
            EncryptorDecryptor plain = new EncryptorDecryptor();

            p.randomGenerator();
            q.randomGenerator();
            e.randomGenerator();

            BigInteger n;
            if (p.MillerRabinTest(10) == true && q.MillerRabinTest(10) == true)
            {
                n = p.result * q.result;
            }
            else
            {
                p.GetNearestPrime();
                q.GetNearestPrime();
                n = p.result * q.result;
            }

            f.EulerFunction(p.result, q.result);
            publicKey = pub_key.GeneratePublicKey(e.result, n);
            privateKey = priv_key.GeneratePrivateKey(e.result);

            Console.WriteLine("Enter your message:");
            string msg = Console.ReadLine();
            BigInteger m = BigInteger.Parse(msg);

            cipher.Encrypt(m, e.result, n);
            Console.WriteLine("The ciphertext is : {0}", cipher.c);

            plain.Decrypt(cipher.c, priv_key.d, n);
            Console.WriteLine("The plaintext is : {0}", plain.m);
            //others code end

        }
    }


    class KeyGenerator
    {
        public BigInteger result, d;
        public BigInteger f;

        public void randomGenerator()                            //create random number
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] randomNumber = new byte[64];
            rng.GetBytes(randomNumber);
            result = new BigInteger(randomNumber);
            result = BigInteger.Abs(result);
        }

        public bool MillerRabinTest(int k)               //primality test
        {
            if (result == 2 || result == 3)
                return true;
            if (result < 2 || result % 2 == 0)
                return false;

            BigInteger d = result - 1;
            int s = 0;

            while (d % 2 == 0)
            {
                d /= 2;
                s += 1;

            }

            for (int i = 0; i < k; i++)
            {
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                byte[] _a = new byte[result.ToByteArray().LongLength];
                BigInteger a;

                do
                {
                    rng.GetBytes(_a);
                    a = new BigInteger(_a);
                }
                while (a < 2 || a >= result - 2);

                BigInteger x = BigInteger.ModPow(a, d, result);

                if (x == 1 || x == result - 1)
                    continue;

                for (int r = 1; r < s; r++)
                {
                    x = BigInteger.ModPow(x, 2, result);

                    if (x == 1)
                        return false;
                    if (x == result - 1)
                        break;
                }

                if (x != result - 1)
                    return false;
            }
            return true;
        }

        public BigInteger GetNearestPrime()                  //get the next prime
        {
            while (MillerRabinTest(10) == false)
            {
                result++;
            }
            return result;
        }

        public void EulerFunction(BigInteger p, BigInteger q)          //euler function: f(n) = (p-1)(q-1)
        {
            BigInteger m = p - 1;
            BigInteger n = q - 1;
            f = m * n;
        }

        public BigInteger GeneratePublicKey(BigInteger e, BigInteger n)        // public key
        {
            while (e > 1)
            {
                BigInteger ef = BigInteger.Pow(e, (int)f);
                ef = 1 % n;
            }
            return e;
        }

        public BigInteger GeneratePrivateKey(BigInteger e)                  //private key
        {
            d = (1 / e) % f;
            return d;
        }
    }

    class EncryptorDecryptor
    {
        public BigInteger c, m;

        public void Encrypt(BigInteger m, BigInteger e, BigInteger n)          //encryption
        {
            c = BigInteger.ModPow(m, e, n);
        }

        public void Decrypt(BigInteger cResult, BigInteger d, BigInteger n)    //decryption
        {
            m = BigInteger.ModPow(cResult, d, n);
        }
    }
}