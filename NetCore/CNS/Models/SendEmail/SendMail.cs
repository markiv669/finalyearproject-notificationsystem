using System.Net.Mail;

namespace CNS.Models.SendEmail
{
    public class SendMail
    {
        public static bool Send(string u_id, string emailID, string randomPassword)
        {
            bool status = false;
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.Credentials = new System.Net.NetworkCredential("kathford.cns@gmail.com", "kathford123");
                //smtpClient.UseDefaultCredentials = true;
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = true;
                MailMessage mail = new MailMessage();
                //Setting From , To and CC
                mail.From = new MailAddress("kathford.cns@gmail.com");
                mail.To.Add(new MailAddress(emailID));
                mail.Body = "HI,  your login credentials are\nusername=" + u_id + "\npassword=" + randomPassword + "\nPlease change your password once you've logged in.";
                mail.Subject = "kathford Credentials";
                smtpClient.Send(mail);
                status = true;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                status=false;
            }
            return status;
        }
    }
}