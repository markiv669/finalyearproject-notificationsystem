using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD
{
    public class AddNewFaculty
    {
        public static bool Add(AddFacultyModel data)
        {
            bool status = false;
            string f_name = data.faculty_name.ToLower();
            string notificationtable, querytable, filetable, eventparticipant, activebatch, batchdata, addFacultyQuery;
            notificationtable = "create TABLE " + f_name + "_notification(n_id VARCHAR(25) PRIMARY KEY,n_date VARCHAR(10) NOT NULL,n_time TIME NOT NULL,n_message text NOT NULL,n_semester int NOT NULL,n_event int NOT NULL,e_location VARCHAR(100),e_deadline VARCHAR(10),e_time TIME,e_title varchar(100),image_count int NOT NULL,file_count  int NOT NULL, n_edit int NOT NULL, latitude double, longitude double) character set 'utf8' collate 'utf8_unicode_ci';";
            querytable = "create TABLE " + f_name + "_query(n_id varchar(25),u_id varchar(15),q_message text NOT NULL,q_date VARCHAR(10),q_time TIME,foreign key (n_id) references kathford.bsccsit_notification (n_id) ON DELETE CASCADE,foreign key (u_id) references kathford.users(u_id) ON DELETE CASCADE) character set 'utf8' collate 'utf8_unicode_ci';";
            filetable = "CREATE TABLE " + f_name + "_file_table(f_id VARCHAR(100) PRIMARY KEY,n_id VARCHAR(25),f_name VARCHAR(200),foreign key (n_id) REFERENCES kathford.bsccsit_notification (n_id) ON DELETE CASCADE)character set 'utf8'collate 'utf8_unicode_ci';";
            eventparticipant = "create table " + f_name + "_event_participant_table(n_id VARCHAR(25),u_id VARCHAR(15),foreign key (n_id) references kathford.bsccsit_notification (n_id) ON DELETE CASCADE ,foreign key (u_id) references kathford.users (u_id) ON DELETE CASCADE ) character set 'utf8' collate 'utf8_unicode_ci';";
            activebatch = "create table " + f_name + "_activebatch(batch_id int PRIMARY KEY) character set 'utf8' collate 'utf8_unicode_ci';";
            addFacultyQuery = "insert into faculty values(@faculty_name,@f_contact,@f_address,@f_information)";
            batchdata = "insert into " + f_name + "_activebatch values(0)";

            MySqlCommand cmd1, cmd2, cmd3, cmd4, cmd5, addFacultyCmd, addbatch;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    cmd1 = connection.CreateCommand();
                    cmd1.CommandText = notificationtable;
                    cmd2 = connection.CreateCommand();
                    cmd2.CommandText = querytable;
                    cmd3 = connection.CreateCommand();
                    cmd3.CommandText = filetable;
                    cmd4 = connection.CreateCommand();
                    cmd4.CommandText = eventparticipant;
                    cmd5 = connection.CreateCommand();
                    cmd5.CommandText = activebatch;

                    addFacultyCmd = new MySqlCommand(addFacultyQuery, connection);

                    try
                    {
                        addFacultyCmd.Parameters.AddWithValue("@faculty_name", f_name.ToUpper());
                        addFacultyCmd.Parameters.AddWithValue("@f_address", data.f_address);
                        addFacultyCmd.Parameters.AddWithValue("@f_contact", data.f_contact);
                        addFacultyCmd.Parameters.AddWithValue("@f_information", data.f_information);
                        addFacultyCmd.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd1.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd2.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd3.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd4.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd5.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }

                    try
                    {
                        addbatch = new MySqlCommand(batchdata, connection);
                        addbatch.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    status = true;
                }
            }
            catch (System.Exception ex)
            {
                status = false;
                System.Console.WriteLine(ex.Message);
            }





            return status;
        }

        public static bool Remove(string faculty_name)
        {
            bool status = false;
            string f_name = faculty_name.ToLower();
            string notificationtable, querytable, filetable, eventparticipant, activebatch, deleteFacultyQuery;
            notificationtable = "drop table kathford." + f_name + "_notification";
            filetable = "drop table kathford." + f_name + "_file_table";
            querytable = "drop table kathford." + f_name + "_query";
            eventparticipant = "drop table kathford." + f_name + "_event_participant_table";
            activebatch = "drop table kathford." + f_name + "_activebatch";
            deleteFacultyQuery = "delete from faculty where faculty_name=@f_name";
            MySqlCommand cmd1, cmd2, cmd3, cmd4, cmd5, deleteFacultyCmd;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    cmd1 = connection.CreateCommand();
                    cmd1.CommandText = notificationtable;
                    cmd2 = connection.CreateCommand();
                    cmd2.CommandText = querytable;
                    cmd3 = connection.CreateCommand();
                    cmd3.CommandText = filetable;
                    cmd4 = connection.CreateCommand();
                    cmd4.CommandText = eventparticipant;
                    cmd5 = connection.CreateCommand();
                    cmd5.CommandText = activebatch;
                    deleteFacultyCmd = new MySqlCommand(deleteFacultyQuery, connection);
                    try
                    {
                        deleteFacultyCmd.Parameters.AddWithValue("@f_name", f_name.ToUpper());
                        deleteFacultyCmd.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd4.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd3.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd2.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd1.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    try
                    {
                        cmd5.ExecuteNonQuery();
                    }
                    catch (System.Exception ex)
                    {
                        System.Console.WriteLine(ex.Message);
                    }
                    status = true;
                }
            }
            catch (System.Exception ex)
            {
                status = false;
                System.Console.WriteLine(ex.Message);
            }

            // if (!status)
            // {
            //     notificationtable = "create TABLE kathford." + f_name + "_notification(n_id VARCHAR(25) PRIMARY KEY,n_date VARCHAR(10) NOT NULL,n_time TIME NOT NULL,n_message text NOT NULL,n_semester int NOT NULL,n_event int NOT NULL,e_location VARCHAR(100),e_deadline VARCHAR(10),e_time TIME,e_title varchar(100),image_count int NOT NULL,file_count  int NOT NULL) character set 'utf8' collate 'utf8_unicode_ci';";
            //     querytable = "create TABLE kathford." + f_name + "_query(n_id varchar(25),u_id varchar(15),q_message text NOT NULL,q_date VARCHAR(10),q_time TIME,foreign key (n_id) references kathford.bsccsit_notification (n_id) ON DELETE CASCADE,foreign key (u_id) references kathford.users(u_id) ON DELETE CASCADE) character set 'utf8' collate 'utf8_unicode_ci';";
            //     filetable = "CREATE TABLE kathford." + f_name + "_file_table(f_id VARCHAR(100) PRIMARY KEY,n_id VARCHAR(25),foreign key (n_id) REFERENCES kathford.bsccsit_notification (n_id) ON DELETE CASCADE)character set 'utf8'collate 'utf8_unicode_ci';";
            //     eventparticipant = "create table kathford." + f_name + "_event_participant_table(n_id VARCHAR(25),u_id VARCHAR(15),foreign key (n_id) references kathford.bsccsit_notification (n_id),foreign key (u_id) references kathford.users (u_id)) character set 'utf8' collate 'utf8_unicode_ci';";
            //     try
            //     {
            //         using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
            //         {
            //             connection.Open();
            //             cmd1 = connection.CreateCommand();
            //             cmd1.CommandText = notificationtable;
            //             cmd2 = connection.CreateCommand();
            //             cmd2.CommandText = querytable;
            //             cmd3 = connection.CreateCommand();
            //             cmd3.CommandText = filetable;
            //             cmd4 = connection.CreateCommand();
            //             cmd4.CommandText = eventparticipant;
            //             cmd1.ExecuteNonQuery();
            //             cmd2.ExecuteNonQuery();
            //             cmd3.ExecuteNonQuery();
            //             cmd4.ExecuteNonQuery();
            //             status = false;
            //         }
            //     }
            //     catch (System.Exception ex)
            //     {
            //         status = false;
            //         System.Console.WriteLine(ex.Message);
            //     }
            // }
            return status;
        }

        public static bool Edit(AddFacultyModel data)
        {
            bool status = false;
            string f_name = data.faculty_name.ToLower();
            string addFacultyQuery = "update faculty set f_contact=@f_contact, f_address=@f_address, f_information=@f_information where faculty_name=@faculty_name";
            MySqlCommand addFacultyCmd;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    addFacultyCmd = new MySqlCommand(addFacultyQuery, connection);
                    try
                    {
                        addFacultyCmd.Parameters.AddWithValue("@faculty_name", f_name.ToUpper());
                        addFacultyCmd.Parameters.AddWithValue("@f_address", data.f_address);
                        addFacultyCmd.Parameters.AddWithValue("@f_contact", data.f_contact);
                        addFacultyCmd.Parameters.AddWithValue("@f_information", data.f_information);
                        addFacultyCmd.ExecuteNonQuery();
                        status = true;
                    }
                    catch (System.Exception ex)
                    {
                        status = false;
                        System.Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (System.Exception ex)
            {
                status = false;
                System.Console.WriteLine(ex.Message);
            }

            return status;
        }
    }
}