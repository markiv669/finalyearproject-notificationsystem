using MySql.Data.MySqlClient;
using CNS.Models.PostData;
using CNS.Models.FileHandeling;
namespace CNS.Models.CRUD
{
    public class InsertFileData
    {
        public static bool Insert(NotificationData data)
        {
            bool status = false;
            string query = "insert into " + data.faculty + "_file_table values(@f_id,@n_id,@f_name)";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = null;
                    for (int i = 0; i < data.file.Count; i++)
                    {
                        command = new MySqlCommand(query, connection);
                        command.Parameters.AddWithValue("@f_id", data.n_id + i + "$" + data.file[i].FileName.ToString());
                        command.Parameters.AddWithValue("@n_id", data.n_id);
                        command.Parameters.AddWithValue("@f_name", data.file[i].FileName.ToLower());
                        if (command.ExecuteNonQuery() == 1)
                        {
                            status = true;
                        }
                        else
                        {
                            status = false;
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }

        public static bool EditInsert(EditNotificationData data, int currentTotalFileCount)
        {
            bool status = false;
            string query = "insert into " + data.faculty + "_file_table values(@f_id,@n_id,@f_name)";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);

            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = null;
                    for (int i = 0; i < data.file.Count; i++)
                    {
                        command = new MySqlCommand(query, connection);
                        command.Parameters.AddWithValue("@f_id", data.n_id + (i + currentTotalFileCount) + "$" + data.file[i].FileName.ToString());
                        command.Parameters.AddWithValue("@n_id", data.n_id);
                        command.Parameters.AddWithValue("@f_name", data.file[i].FileName.ToLower());
                        if (command.ExecuteNonQuery() == 1)
                        {
                            status = true;
                        }
                        else
                        {
                            status = false;
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }
    }
}