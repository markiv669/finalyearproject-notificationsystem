using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD
{
    public class InsertDeviceToken
    {
        public static bool Insert(FcmToken token)
        {
            bool status = false;
            string query = "insert into device_token values(@u_id,@token)";
            using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
            {
                System.Console.WriteLine(token.tokenID);
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@token", token.tokenID);
                    command.Parameters.AddWithValue("@u_id", token.u_id);
                    if (command.ExecuteNonQuery() > 0)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    string query2 = "update device_token set u_id=@u_id where t_id=@token";
                    System.Console.WriteLine(token.tokenID);
                    try
                    {
                        MySqlCommand command = new MySqlCommand(query2, connection);
                        command.Parameters.AddWithValue("@token", token.tokenID);
                        command.Parameters.AddWithValue("@u_id", token.u_id);
                        if (command.ExecuteNonQuery() > 0)
                        {
                            status = true;
                        }
                    }
                    catch (System.Exception ex2)
                    {
                        System.Console.WriteLine(ex2.Message);
                        status = false;
                    }

                }
                return status;
            }
        }
    }
}