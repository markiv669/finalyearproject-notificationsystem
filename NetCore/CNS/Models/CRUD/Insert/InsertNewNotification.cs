using MySql.Data.MySqlClient;
using MySql.Data;
using CNS.Models.PostData;


namespace CNS.Models.CRUD
{
    public class InsertNewNotification
    {
        public static bool Insert(NotificationData data)
        {
            bool status = false;
            MySqlConnection con = new MySqlConnection(DatabaseConnection.connectionstring);
            string faculty = data.faculty.ToLower() + "_notification";
            string query = "Insert into " + faculty + " values(@n_id, @n_date, @n_time, @n_message, @n_semester, @n_event, @e_location, @e_deadline, @e_time, @n_title,@image_count,@file_count,@n_edit,@latitude,@longitude)";
            try
            {
                using (con)
                {
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@n_id", data.n_id);
                    cmd.Parameters.AddWithValue("@n_date", data.n_date);
                    cmd.Parameters.AddWithValue("@n_time", data.n_time);
                    cmd.Parameters.AddWithValue("@n_message", data.n_message);
                    cmd.Parameters.AddWithValue("@n_semester", data.n_semester);
                    cmd.Parameters.AddWithValue("@n_event", data.n_event);
                    cmd.Parameters.AddWithValue("@e_location", data.e_location);
                    cmd.Parameters.AddWithValue("@e_deadline", data.e_deadline);
                    cmd.Parameters.AddWithValue("@e_time", data.e_time);
                    cmd.Parameters.AddWithValue("@n_title", data.n_title);
                    cmd.Parameters.AddWithValue("@file_count", data.file_count);
                    cmd.Parameters.AddWithValue("@image_count", data.image_count);
                    cmd.Parameters.AddWithValue("@n_edit", 0);
                    if (data.n_event == 1)
                    {
                        cmd.Parameters.AddWithValue("@latitude", data.latitude);
                        cmd.Parameters.AddWithValue("@longitude", data.longitude);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@latitude", 0);
                        cmd.Parameters.AddWithValue("@longitude", 0);
                    }
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        status = true;
                    }


                }

            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            return status;
        }
    }
}