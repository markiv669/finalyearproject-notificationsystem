using MySql.Data.MySqlClient;
using MySql.Data;
using CNS.Models.PostData;
using CNS.Models.FileHandeling;

namespace CNS.Models.CRUD
{
    public class InsertNewUser
    {
        public static bool Insert(UserSignupData data, string password)
        {
            bool status = false;

            string constr = DatabaseConnection.connectionstring;
            MySqlConnection con = new MySqlConnection(constr);
            string query = "insert into users values(@u_id,@u_fname,@u_mname,@u_lname,@u_semester,@u_faculty,@u_phone,md5(@hideword),@father_fname,@father_mname,@father_phone,@mother_fname,@mother_mname,@mother_phone,@primary_email,@alternate_email,@passwordChangedStatus,@image,@authority,@u_paddress,@u_taddress);";
            try
            {
                using (con)
                {
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@u_id", data.u_id);
                    cmd.Parameters.AddWithValue("@u_fname", data.u_fname);
                    cmd.Parameters.AddWithValue("@u_mname", data.u_mname);
                    cmd.Parameters.AddWithValue("@u_lname", data.u_lname);
                    cmd.Parameters.AddWithValue("@u_semester", data.u_semester);
                    cmd.Parameters.AddWithValue("@u_faculty", data.u_faculty);
                    cmd.Parameters.AddWithValue("@u_phone", data.u_phone);
                    cmd.Parameters.AddWithValue("@father_fname", data.father_fname);
                    cmd.Parameters.AddWithValue("@father_mname", data.father_mname);
                    cmd.Parameters.AddWithValue("@father_phone", data.father_phone);
                    cmd.Parameters.AddWithValue("@mother_fname", data.mother_fname);
                    cmd.Parameters.AddWithValue("@mother_mname", data.mother_mname);
                    cmd.Parameters.AddWithValue("@mother_phone", data.mother_phone);
                    cmd.Parameters.AddWithValue("@primary_email", data.primary_email);
                    cmd.Parameters.AddWithValue("@alternate_email", data.alternate_email);
                    cmd.Parameters.AddWithValue("@hideword", password);
                    cmd.Parameters.AddWithValue("@passwordChangedStatus", 0);
                    cmd.Parameters.AddWithValue("@image", data.u_id + "." + StoreFile.GetFileExtension(data.image.FileName));
                    cmd.Parameters.AddWithValue("@authority", data.authority);
                    cmd.Parameters.AddWithValue("@u_taddress", data.u_taddress);
                    cmd.Parameters.AddWithValue("@u_paddress", data.u_paddress);
                    int effectedRowCount = cmd.ExecuteNonQuery();
                    if (effectedRowCount > 0)
                    {
                        status = true;
                    }
                }
            }

            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                status = false;

            }
            return status;



        }

    }

}