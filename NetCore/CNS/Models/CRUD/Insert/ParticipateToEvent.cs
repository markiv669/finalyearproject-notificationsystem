using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD.Insert
{
    public class ParticipateToEvent
    {
        public static bool Insert(EventParticipantsData data)
        {
            bool status = false;
            //string query = "insert into " + data.u_faculty + "_query values(@n_id,@u_id,@q_message,@q_date,@q_time)";
            string faculty = data.faculty.ToLower() + "_event_participant_table";
            string query = "insert into " + faculty + " values(@n_id,@u_id)";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    command.Parameters.AddWithValue("@u_id", data.u_id);
                    int effectedRowCount = command.ExecuteNonQuery();
                    if (effectedRowCount > 0)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }
    }
}