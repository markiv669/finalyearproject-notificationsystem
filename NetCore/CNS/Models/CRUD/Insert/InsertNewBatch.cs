using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Insert
{
    public class InsertNewBatch
    {
        public static bool Insert(AddBatchData data)
        {
            bool status = false;

            string query = "insert into " + data.faculty.ToLower() + "_activebatch value(@batch_id)";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@batch_id", data.batch);
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        status = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                status = false;
            }

            return status;
        }
    }
}