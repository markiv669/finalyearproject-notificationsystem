using MySql.Data.MySqlClient;
using CNS.Models.PostData;
using System.Data;

namespace CNS.Models.CRUD
{
    public class InsertForgotPassword
    {
        public static bool Insert(ForgotPasswordData data, string randomPassword)
        {
            bool status = false;
            string connectionString = DatabaseConnection.connectionstring;
            MySqlConnection connection = new MySqlConnection(connectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command=new MySqlCommand("forgotpasswordreset",connection);
                    command.CommandType=CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userid",data.u_id);
                    command.Parameters.AddWithValue("@email",data.email);
                    command.Parameters.AddWithValue("@randompassword",randomPassword);
                    int sucess=(int)command.ExecuteScalar();
                    if(sucess==2)
                    {
                        status=true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }

            }

            return status;
        }
    }
}