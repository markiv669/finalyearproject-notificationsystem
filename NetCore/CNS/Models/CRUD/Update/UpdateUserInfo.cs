using MySql.Data.MySqlClient;
using CNS.Models.PostData;
using CNS.Models.FileHandeling;

namespace CNS.Models.CRUD
{
    public class UpdateUserInfo
    {
        public static bool Update(UserSignupData data)
        {
            bool status = false;

            string query = "update users set u_fname=@u_fname,u_mname=@u_mname,u_lname=@u_lname,u_semester=@u_semester,u_faculty=@u_faculty,u_phone=@u_phone,father_fname=@father_fname,father_mname=@father_mname,father_phone=@father_phone,mother_fname=@mother_fname,mother_mname=@mother_mname,mother_phone=@mother_phone,primary_email=@primary_email,alternate_email=@alternate_email,image=@image,authority=@authority,u_paddress=@u_paddress,u_taddress=@u_taddress where u_id=@u_id";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@u_id", data.u_id);
                    cmd.Parameters.AddWithValue("@u_fname", data.u_fname);
                    cmd.Parameters.AddWithValue("@u_mname", data.u_mname);
                    cmd.Parameters.AddWithValue("@u_lname", data.u_lname);
                    cmd.Parameters.AddWithValue("@u_semester", data.u_semester);
                    cmd.Parameters.AddWithValue("@u_faculty", data.u_faculty);
                    cmd.Parameters.AddWithValue("@u_phone", data.u_phone);
                    cmd.Parameters.AddWithValue("@father_fname", data.father_fname);
                    cmd.Parameters.AddWithValue("@father_mname", data.father_mname);
                    cmd.Parameters.AddWithValue("@father_phone", data.father_phone);
                    cmd.Parameters.AddWithValue("@mother_fname", data.mother_fname);
                    cmd.Parameters.AddWithValue("@mother_mname", data.mother_mname);
                    cmd.Parameters.AddWithValue("@mother_phone", data.mother_phone);
                    cmd.Parameters.AddWithValue("@primary_email", data.primary_email);
                    cmd.Parameters.AddWithValue("@alternate_email", data.alternate_email);


                    cmd.Parameters.AddWithValue("@image", data.u_id + "." + StoreFile.GetFileExtension(data.image.FileName));
                    cmd.Parameters.AddWithValue("@authority", data.authority);
                    cmd.Parameters.AddWithValue("@u_paddress", data.u_paddress);
                    cmd.Parameters.AddWithValue("@u_taddress", data.u_taddress);

                    int effectedRowCount = cmd.ExecuteNonQuery();
                    if (effectedRowCount == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}