using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD
{
    public class UpdateQuery
    {
        public static bool Update(QueryData data)
        {
            bool status = false;
            string faculty = data.faculty.ToLower() + "_query";
            string query = "update " + faculty + " set q_message=@q_message,q_date=CURDATE(),q_time=CURTIME() where n_id=@n_id and u_id=@u_id and q_time=@q_time";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@q_message", data.q_message);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    command.Parameters.AddWithValue("@u_id", data.u_id);
                    command.Parameters.AddWithValue("@q_time", data.q_time);
                    int effectedRowCount = command.ExecuteNonQuery();
                    if (effectedRowCount == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}