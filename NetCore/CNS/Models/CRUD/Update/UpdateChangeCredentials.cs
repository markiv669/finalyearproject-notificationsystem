using MySql.Data.MySqlClient;
using CNS.Models.PostData;
namespace CNS.Models.CRUD
{
    public class UpdateCredentials
    {
        public static bool UpdateUserCredentials(CredentialsChangeData data)
        {
            bool status = false;
            string connectionString = DatabaseConnection.connectionstring;
            string query = "update users set hideword=MD5(@newPassword),passwordchangedstatus=1 where u_id=@u_id and hideword=MD5(@oldPassword)";
            MySqlConnection connection = new MySqlConnection(connectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@newPassword", data.newPassword);
                    command.Parameters.AddWithValue("@oldPassword", data.oldPassword);
                    command.Parameters.AddWithValue("@u_id", data.u_id);
                    int effectedRowCount = command.ExecuteNonQuery();
                    if (effectedRowCount == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}