using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Update
{
    public class UpdateNotification
    {
        public static bool Edit(EditNotificationData data)
        {
            bool status = false;
            MySqlConnection con = new MySqlConnection(DatabaseConnection.connectionstring);
            string faculty = data.faculty.ToLower() + "_notification";
            string query = "update " + faculty + " set n_message=@n_message, n_semester=@n_semester,n_event=@n_event, e_location=@e_location, e_deadline=@e_deadline, e_time=@e_time, e_title=@n_title,image_count=@image_count,file_count=@file_count,n_edit=1 where n_id=@n_id";
            try
            {
                using (con)
                {
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@n_id", data.n_id);
                    cmd.Parameters.AddWithValue("@n_date", data.n_date);
                    cmd.Parameters.AddWithValue("@n_time", data.n_time);
                    cmd.Parameters.AddWithValue("@n_message", data.n_message);
                    cmd.Parameters.AddWithValue("@n_semester", data.n_semester);
                    cmd.Parameters.AddWithValue("@n_event", data.n_event);
                    cmd.Parameters.AddWithValue("@e_location", data.e_location);
                    cmd.Parameters.AddWithValue("@e_deadline", data.e_deadline);
                    cmd.Parameters.AddWithValue("@e_time", data.e_time);
                    cmd.Parameters.AddWithValue("@n_title", data.n_title);
                    cmd.Parameters.AddWithValue("@file_count", data.file_count);
                    cmd.Parameters.AddWithValue("@image_count", data.image_count);
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        status = true;
                    }


                }

            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            return status;
        }
    }
}
