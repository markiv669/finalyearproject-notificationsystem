using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD
{
    public class AdminTokenRead
    {
        public static List<string> Read(string faculty)
        {

            string[] u_id = null;
            List<string> t_id = new List<string>();
            string adminlistQuery = "select u_id from users where u_faculty=@u_faculty and authority=1";
            string tokenQuery = "select t_id from device_token where u_id=@u_id ";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {

                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(adminlistQuery, connection);
                    cmd.Parameters.AddWithValue("@u_faculty", faculty);

                    using (MySqlDataAdapter userAdapter = new MySqlDataAdapter(cmd))
                    {
                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        userAdapter.Fill(dataTable);
                        int count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            u_id = new string[count];
                        }
                        for (int i = 0; i < count; i++)
                        {
                            u_id[i] = dataTable.Rows[i]["u_id"].ToString();
                        };
                    }

                    if (u_id != null && u_id.Length != 0)
                    {
                        MySqlCommand cmd2 = new MySqlCommand(tokenQuery, connection);

                        for (int i = 0; i < u_id.Length; i++)
                        {
                            cmd2.Parameters.AddWithValue("@u_id", u_id[i]);
                            using (MySqlDataAdapter tokenAdapter = new MySqlDataAdapter(cmd2))
                            {
                                DataTable dataTable = new DataTable();
                                DataTable fileDataTable = new DataTable();
                                tokenAdapter.Fill(dataTable);
                                int count = dataTable.Rows.Count;

                                for (int j = 0; j < count; j++)
                                {
                                    t_id.Add(dataTable.Rows[j]["t_id"].ToString());
                                }
                            }
                            cmd2.Parameters.Clear();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            return t_id;
        }
    }
}