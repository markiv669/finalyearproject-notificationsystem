using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD
{
    public class UserAuthorityVerification
    {
        public static bool ToAlterNotification(NotificationData data)
        {
            bool status = false;
            string query = "select count(u_id) from users where u_id=@u_id and hideword=MD5(@hideword) and authority=1";
            string connectionString = DatabaseConnection.connectionstring;
            MySqlConnection connection = new MySqlConnection(connectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@hideword", data.admin_password);
                    command.Parameters.AddWithValue("@u_id", data.admin_id);
                    if ((long)command.ExecuteScalar() == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }
        public static bool ToEditNotification(EditNotificationData data)
        {
            bool status = false;
            string query = "select count(u_id) from users where u_id=@u_id and hideword=MD5(@hideword) and authority=1";
            string connectionString = DatabaseConnection.connectionstring;
            MySqlConnection connection = new MySqlConnection(connectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@hideword", data.admin_password);
                    command.Parameters.AddWithValue("@u_id", data.admin_id);
                    if ((long)command.ExecuteScalar() == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }

        public static bool ToAlterUser(UserSignupData data)
        {
            bool status = false;
            string query = "select count(u_id) from users where u_id=@u_id  and hideword=MD5(@hideword) and authority=2";
            string connectionString = DatabaseConnection.connectionstring;
            MySqlConnection connection = new MySqlConnection(connectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@u_id", data.admin_id);
                    command.Parameters.AddWithValue("@hideword", data.admin_password);
                    if ((long)command.ExecuteScalar() == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }
    }
}