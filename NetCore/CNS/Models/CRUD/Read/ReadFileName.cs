using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class ReadFileName
    {
        public static string Read(string f_id, string faculty)
        {
            string filename = "";
            string query = "select f_name from " + faculty.ToLower() + "_file_table where f_id=@f_id";
            MySqlDataReader reader = null;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@f_id", f_id);


                    using (reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                filename = reader.GetValue(0).ToString();

                            }
                        }
                    }


                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return filename;
        }
    }
}