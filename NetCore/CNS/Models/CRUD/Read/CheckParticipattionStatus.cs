using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class EventParticipationRead
    {
        public static int CheckParticipationStatus(string u_id, string faculty, string n_id)
        {
            int status = 0;
            string query = "select count(u_id) from " + faculty.ToLower() + "_event_participant_table where u_id=@u_id and n_id=@n_id";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@u_id", u_id);
                    command.Parameters.AddWithValue("@n_id", n_id);
                    System.Int64 count = (System.Int64)command.ExecuteScalar();
                    if (count == 1)
                    {
                        status = 1;
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                status = 2; //id client receives 2 as status of participation they have to resend the request
            }


            return status;
        }
    }
}