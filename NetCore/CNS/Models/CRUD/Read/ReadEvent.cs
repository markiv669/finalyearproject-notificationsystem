using MySql.Data.MySqlClient;
using CNS.Models.PostData;
using System.Data;
using CNS.Models.FileHandeling;

namespace CNS.Models.CRUD.Read
{
    public class ReadEvent
    {
        public static NotificationData[] Read(EventParticipantsData data)
        {

            string faculty = data.faculty.ToLower();
            string notificationQuery = "select * from " + faculty + "_notification where n_id IN (select n_id from " + faculty + "_event_participant_table where u_id=@u_id ) ";
            string fileQuery = "select * from " + faculty + "_file_table where n_id=@file_notification_id";
            NotificationData[] fetchedNotification = null;

            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);

            int count = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand notificationReadCommand = new MySqlCommand(notificationQuery, connection);
                    MySqlCommand filenameReadCommand = new MySqlCommand(fileQuery, connection);

                    notificationReadCommand.Parameters.AddWithValue("@u_id", data.u_id);

                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(notificationReadCommand))
                    {
                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            fetchedNotification = new NotificationData[count];
                            for (int i = 0; i < count; i++)
                            {
                                fetchedNotification[i] = new NotificationData();
                            }
                            for (int index = 0; index < count; index++)
                            {
                                fetchedNotification[index].n_id = dataTable.Rows[index]["n_id"].ToString();
                                fetchedNotification[index].n_date = dataTable.Rows[index]["n_date"].ToString();
                                fetchedNotification[index].n_time = dataTable.Rows[index]["n_time"].ToString();
                                fetchedNotification[index].n_message = dataTable.Rows[index]["n_message"].ToString();
                                fetchedNotification[index].n_semester = (int)dataTable.Rows[index]["n_semester"];
                                fetchedNotification[index].n_event = (int)dataTable.Rows[index]["n_event"];
                                if (fetchedNotification[index].n_event == 1)
                                {
                                    fetchedNotification[index].latitude = (double)dataTable.Rows[index]["latitude"];
                                    fetchedNotification[index].longitude = (double)dataTable.Rows[index]["longitude"];
                                    fetchedNotification[index].e_participate_status = EventParticipationRead.CheckParticipationStatus(data.u_id, data.faculty, fetchedNotification[index].n_id);
                                }
                                fetchedNotification[index].e_location = dataTable.Rows[index]["e_location"].ToString();
                                fetchedNotification[index].e_deadline = dataTable.Rows[index]["e_deadline"].ToString();
                                fetchedNotification[index].e_time = dataTable.Rows[index]["e_time"].ToString();
                                fetchedNotification[index].n_title = dataTable.Rows[index]["e_title"].ToString();
                                fetchedNotification[index].image_count = (int)dataTable.Rows[index]["image_count"];
                                fetchedNotification[index].file_count = (int)dataTable.Rows[index]["file_count"];
                                if (fetchedNotification[index].image_count > 0 || fetchedNotification[index].file_count > 0)
                                {
                                    fetchedNotification[index].file_name = new string[fetchedNotification[index].file_count];
                                    fetchedNotification[index].image_name = new string[fetchedNotification[index].image_count];
                                    filenameReadCommand.Parameters.AddWithValue("@file_notification_id", fetchedNotification[index].n_id);
                                    using (MySqlDataAdapter fileAdapter = new MySqlDataAdapter(filenameReadCommand))
                                    {
                                        string fileName = null;
                                        string fileExtension = null;
                                        fileAdapter.Fill(fileDataTable);
                                        int fileCount = fileDataTable.Rows.Count;
                                        for (int i = 0, image_index = 0, file_index = 0; i < fileCount; i++)
                                        {
                                            fileName = fileDataTable.Rows[i]["f_id"].ToString();
                                            fileExtension = StoreFile.GetFileExtension(fileName);
                                            if (fileExtension == "jpg" || fileExtension == "jpeg")
                                            {
                                                fetchedNotification[index].image_name[image_index] = fileName;
                                                image_index++;
                                            }
                                            else
                                            {
                                                fetchedNotification[index].file_name[file_index] = fileName;
                                                file_index++;
                                            }
                                        }
                                        fileDataTable.Clear();
                                    }
                                }
                                filenameReadCommand.Parameters.Clear();
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }

            return fetchedNotification;
        }
    }
}