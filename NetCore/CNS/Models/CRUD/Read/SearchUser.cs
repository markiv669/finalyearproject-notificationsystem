using System.Data;
using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class SearchUser
    {
        public static UserSignupData[] Search(UserSearchModel data)
        {
            UserSignupData[] fetchedData = null;

            using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
            {
                try
                {
                    connection.Open();
                    MySqlCommand usersReadCommand = new MySqlCommand(data.finalQuery, connection);

                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(usersReadCommand))
                    {
                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        int count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            fetchedData = new UserSignupData[count];
                            for (int i = 0; i < count; i++)
                            {
                                fetchedData[i] = new UserSignupData();
                            }
                            for (int index = 0; index < count; index++)
                            {
                                fetchedData[index].u_id = dataTable.Rows[index]["u_id"].ToString();
                                fetchedData[index].u_fname = dataTable.Rows[index]["u_fname"].ToString();
                                fetchedData[index].u_mname = dataTable.Rows[index]["u_mname"].ToString();
                                fetchedData[index].u_lname = dataTable.Rows[index]["u_lname"].ToString();
                                fetchedData[index].u_semester = (int)dataTable.Rows[index]["u_semester"];
                                fetchedData[index].u_faculty = dataTable.Rows[index]["u_faculty"].ToString();
                                fetchedData[index].u_phone = dataTable.Rows[index]["u_phone"].ToString();
                                fetchedData[index].father_fname = dataTable.Rows[index]["father_fname"].ToString();
                                fetchedData[index].father_mname = dataTable.Rows[index]["father_mname"].ToString();
                                fetchedData[index].father_phone = dataTable.Rows[index]["father_phone"].ToString();
                                fetchedData[index].mother_fname = dataTable.Rows[index]["mother_fname"].ToString();
                                fetchedData[index].mother_mname = dataTable.Rows[index]["mother_mname"].ToString();
                                fetchedData[index].mother_phone = dataTable.Rows[index]["mother_phone"].ToString();
                                fetchedData[index].primary_email = dataTable.Rows[index]["primary_email"].ToString();
                                fetchedData[index].alternate_email = dataTable.Rows[index]["alternate_email"].ToString();
                                fetchedData[index].passwordChangedStatus = (int)dataTable.Rows[index]["passwordChangedStatus"];
                                fetchedData[index].profilePicture = dataTable.Rows[index]["image"].ToString();
                                fetchedData[index].authority = (int)dataTable.Rows[index]["authority"];
                                fetchedData[index].u_paddress = dataTable.Rows[index]["u_paddress"].ToString();
                                fetchedData[index].u_taddress = dataTable.Rows[index]["u_taddress"].ToString();
                                fetchedData[index].code = 1;
                                fetchedData[index].message = "sucess";
                            }
                        }
                        else
                        {

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return fetchedData;
        }
    }
}