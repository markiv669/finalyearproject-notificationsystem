using System.Data;
using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class ReadQuery
    {
        public static QueryData[] Read(QueryReadData data)
        {
            QueryData[] fetchedQuery = null;
            string faculty = data.faculty.ToLower() + "_query";
            string query = "select * from " + faculty + " where n_id=@n_id  ORDER BY n_id DESC limit @offset , 20 ";
            string usernameQuery = "select u_fname,u_faculty from users where u_id=@u_id";

            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);

            int count = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    MySqlCommand usernamecmd = new MySqlCommand(usernameQuery, connection);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    command.Parameters.AddWithValue("@offset", data.offset);
                    MySqlDataReader reader = null;
                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(command))
                    {
                        DataTable dataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            fetchedQuery = new QueryData[count];
                            for (int i = 0; i < count; i++)
                            {
                                fetchedQuery[i] = new QueryData();
                            }
                            for (int index = 0; index < count; index++)
                            {
                                fetchedQuery[index].n_id = dataTable.Rows[index]["n_id"].ToString();
                                fetchedQuery[index].u_id = dataTable.Rows[index]["u_id"].ToString();
                                fetchedQuery[index].q_message = dataTable.Rows[index]["q_message"].ToString();
                                fetchedQuery[index].q_date = dataTable.Rows[index]["q_date"].ToString();
                                fetchedQuery[index].q_time = dataTable.Rows[index]["q_time"].ToString();
                                usernamecmd.Parameters.AddWithValue("@u_id", fetchedQuery[index].u_id);

                                using (reader = usernamecmd.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            fetchedQuery[index].u_name = reader.GetValue(0).ToString();
                                            fetchedQuery[index].faculty = reader.GetValue(1).ToString();
                                        }
                                    }
                                }
                                reader = null;
                                usernamecmd.Parameters.Clear();
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return fetchedQuery;
        }
    }
}