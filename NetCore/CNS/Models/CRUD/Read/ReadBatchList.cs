using System.Data;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class ReadBatchList
    {
        public static string[] Read(string faculty)
        {
            string[] batchList = null;
            string query = "select * from " + faculty.ToLower() + "_activebatch";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(cmd))
                    {
                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        int count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            batchList = new string[count];
                        }
                        for (int i = 0; i < count; i++)
                        {
                            batchList[i] = dataTable.Rows[i]["batch_id"].ToString();
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            return batchList;
        }
    }
}