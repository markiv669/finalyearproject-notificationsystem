using MySql.Data.MySqlClient;
using CNS.Models.PostData;
using System.Data;

namespace CNS.Models.CRUD
{
    public class LoginCredentialsRead
    {
        //Check if user login credentials are present in database
        //if present the return the complete row
        // public static UserSignupData Login(UserLoginData user)
        // {
        //     UserSignupData fetchedData = new UserSignupData();
        //     string connectionString = DatabaseConnection.connectionstring;
        //     // string query = "select * from users where u_id=@username and hideword=md5(@password)";
        //     MySqlConnection connection = new MySqlConnection(connectionString);
        //     MySqlDataReader reader = null;
        //     using (connection)
        //     {
        //         try
        //         {

        //             connection.Open();

        //             MySqlCommand command = new MySqlCommand("logincheck", connection);
        //             command.CommandType = CommandType.StoredProcedure;
        //             command.Parameters.AddWithValue("@userid", user.u_id);
        //             command.Parameters.AddWithValue("@password", user.password);
        //             using (reader = command.ExecuteReader())
        //             {
        //                 if (reader.HasRows)
        //                 {
        //                     while (reader.Read())
        //                     {
        //                         fetchedData.u_id = reader.GetValue(0).ToString();
        //                         fetchedData.u_fname = reader.GetValue(1).ToString();
        //                         fetchedData.u_mname = reader.GetValue(2).ToString();
        //                         fetchedData.u_lname = reader.GetValue(3).ToString();
        //                         fetchedData.u_semester = reader.GetInt32(4);
        //                         fetchedData.u_faculty = reader.GetValue(5).ToString();
        //                         fetchedData.u_phone = reader.GetValue(6).ToString();
        //                         fetchedData.father_fname = reader.GetValue(8).ToString();
        //                         fetchedData.father_mname = reader.GetValue(9).ToString();
        //                         fetchedData.father_phone = reader.GetValue(10).ToString();
        //                         fetchedData.mother_fname = reader.GetValue(11).ToString();
        //                         fetchedData.mother_mname = reader.GetValue(12).ToString();
        //                         fetchedData.mother_phone = reader.GetValue(13).ToString();
        //                         fetchedData.primary_email = reader.GetValue(14).ToString();
        //                         fetchedData.alternate_email = reader.GetValue(15).ToString();
        //                         fetchedData.passwordChangedStatus = reader.GetInt32(16);
        //                         fetchedData.profilePicture = reader.GetValue(17).ToString();
        //                         fetchedData.authority = reader.GetInt32(18);
        //                         fetchedData.u_paddress = reader.GetValue(19).ToString();
        //                         fetchedData.u_taddress = reader.GetValue(20).ToString();
        //                         fetchedData.code = 1;
        //                         fetchedData.message = "sucess";
        //                     }
        //                 }
        //                 else
        //                 {
        //                     fetchedData.code = 0;
        //                     fetchedData.message = "Invalid user information.\nCheck Username and Password";
        //                 }
        //             }
        //         }
        //         catch (System.Exception ex)
        //         {
        //             System.Console.WriteLine("................\n\n\n\nException Message={0}\n\n\n\n........", ex.Message);
        //             fetchedData.code = 0;
        //             fetchedData.message = ex.Message;
        //         }
        //     }
        //     return fetchedData;
        // }

        public static UserSignupData LoginCheck(UserLoginData user)
        {
            UserSignupData fetchedData = null;
            using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand("logincheck", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userid", user.u_id);
                    command.Parameters.AddWithValue("@password", user.password);
                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(command))
                    {

                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        fetchedData = new UserSignupData();
                        int count = dataTable.Rows.Count;
                        if (count == 1)
                        {

                            for (int index = 0; index < count; index++)
                            {

                                fetchedData.u_id = dataTable.Rows[0]["u_id"].ToString();
                                fetchedData.u_fname = dataTable.Rows[0]["u_fname"].ToString();
                                fetchedData.u_mname = dataTable.Rows[0]["u_mname"].ToString();
                                fetchedData.u_lname = dataTable.Rows[0]["u_lname"].ToString();
                                fetchedData.authority = (int)dataTable.Rows[0]["authority"];
                                if (fetchedData.authority == 1)
                                {
                                    fetchedData.u_faculty = dataTable.Rows[0]["u_faculty"].ToString();
                                }
                                else if (fetchedData.authority == 0)
                                {
                                    fetchedData.u_semester = (int)dataTable.Rows[0]["u_semester"];
                                    fetchedData.u_faculty = dataTable.Rows[0]["u_faculty"].ToString();
                                    fetchedData.father_fname = dataTable.Rows[0]["father_fname"].ToString();
                                    fetchedData.father_mname = dataTable.Rows[0]["father_mname"].ToString();
                                    fetchedData.father_phone = dataTable.Rows[0]["father_phone"].ToString();
                                    fetchedData.mother_fname = dataTable.Rows[0]["mother_fname"].ToString();
                                    fetchedData.mother_mname = dataTable.Rows[0]["mother_mname"].ToString();
                                    fetchedData.mother_phone = dataTable.Rows[0]["mother_phone"].ToString();
                                }
                                else
                                {

                                }
                                fetchedData.u_phone = dataTable.Rows[0]["u_phone"].ToString();
                                fetchedData.primary_email = dataTable.Rows[0]["primary_email"].ToString();
                                fetchedData.alternate_email = dataTable.Rows[0]["alternate_email"].ToString();
                                fetchedData.passwordChangedStatus = (int)dataTable.Rows[0]["passwordChangedStatus"];
                                fetchedData.profilePicture = dataTable.Rows[0]["image"].ToString();

                                fetchedData.u_paddress = dataTable.Rows[0]["u_paddress"].ToString();
                                fetchedData.u_taddress = dataTable.Rows[0]["u_taddress"].ToString();
                                fetchedData.code = 1;
                                fetchedData.message = "sucess";
                            }
                        }
                        else
                        {
                            fetchedData.code = 0;
                            fetchedData.message = "invalid username or password";
                        }

                    }
                }
                catch (System.Exception ex)
                {
                    fetchedData.code = 0;
                    fetchedData.message = ex.Message;
                    System.Console.WriteLine(ex.Message);
                }
            }
            return fetchedData;
        }
    }
}