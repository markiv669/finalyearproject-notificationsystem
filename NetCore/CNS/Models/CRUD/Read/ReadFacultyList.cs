using System.Data;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Read
{
    public class ReadFacultyList
    {
        public static string[] Read()
        {
            string[] faculty_list = null;
            string query = "select * from faculty where faculty_name<>'none'";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    using (MySqlDataAdapter notificationAdapter = new MySqlDataAdapter(cmd))
                    {
                        DataTable dataTable = new DataTable();
                        DataTable fileDataTable = new DataTable();
                        notificationAdapter.Fill(dataTable);
                        int count = dataTable.Rows.Count;
                        if (count > 0)
                        {
                            faculty_list = new string[count];
                        }
                        for (int i = 0; i < count; i++)
                        {
                            faculty_list[i] = dataTable.Rows[i]["faculty_name"].ToString();
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            return faculty_list;
        }
    }
}