using MySql.Data.MySqlClient;
using CNS.Models.PostData;
namespace CNS.Models.CRUD
{
    public class DeleteNotification
    {
        public static bool Delete(NotificationData data)
        {
            bool status = false;
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            string faculty = data.faculty.ToLower() + "_notification";
            string query = "delete from " + faculty + " where n_id=@n_id";
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    if (command.ExecuteNonQuery() > 0)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}