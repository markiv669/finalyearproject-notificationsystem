using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD
{
    public class DeleteForgotPassword
    {
        public static bool Delete(string u_id)
        {
            bool status = false;
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand("delete from forgotpassword where u_id=@u_id", connection);
                    command.Parameters.AddWithValue("@u_id", u_id);
                    if (command.ExecuteNonQuery() > 0)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}