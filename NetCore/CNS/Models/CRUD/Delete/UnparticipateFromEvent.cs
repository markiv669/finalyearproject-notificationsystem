using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD.Delete
{
    public class UnparticipateFromEvent
    {
        public static bool Delete(EventParticipantsData data)
        {
            bool status = false;

            string faculty = data.faculty.ToLower() + "_event_participant_table";
            string query = "Delete from " + faculty + " where n_id=@n_id and u_id=@u_id";
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    command.Parameters.AddWithValue("@u_id", data.u_id);
                    int effectedRowCount = command.ExecuteNonQuery();
                    if (effectedRowCount > 0)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                    status = false;
                }
            }
            return status;
        }
    }
}