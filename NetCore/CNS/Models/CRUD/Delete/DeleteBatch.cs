using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Delete
{
    public class DeleteBatch
    {
        public static bool Delete(AddBatchData data)
        {
            bool status = false;
            string query = "delete from " + data.faculty.ToLower() + "_activebatch where batch_id=@batch_id";
            string deleteUser = "delete from users where u_semester=@batch_id";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand cmd1 = new MySqlCommand(query, connection);
                    cmd1.Parameters.AddWithValue("@batch_id", data.batch);
                    if (cmd1.ExecuteNonQuery() > 0)
                    {
                        status = true;
                        MySqlCommand cmd2 = new MySqlCommand(deleteUser, connection);
                        cmd2.Parameters.AddWithValue("@batch_id", data.batch);
                        cmd2.ExecuteNonQuery();
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            return status;
        }
    }
}