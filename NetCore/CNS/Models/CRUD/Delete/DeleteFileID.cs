using CNS.Models.PostData;
using MySql.Data.MySqlClient;

namespace CNS.Models.CRUD.Delete
{
    public class DeleteFileID
    {
        public static bool Delete(EditNotificationData data)
        {
            bool status = false;
            string query = "delete from " + data.faculty.ToLower() + "_file_table where f_id=@f_id";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    for (int i = 0; i < data.delete_file_name.Count; i++)
                    {
                        cmd.Parameters.AddWithValue("@f_id", data.delete_file_name[i].ToString());
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    status = true;
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                status = false;
            }
            return status;
        }
    }
}