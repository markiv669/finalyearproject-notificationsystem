using MySql.Data.MySqlClient;
using CNS.Models.PostData;

namespace CNS.Models.CRUD.Delete
{
    public class DeleteQuery
    {
        public static bool Delete(QueryData data)
        {
            bool status = false;
            MySqlConnection connection = new MySqlConnection(DatabaseConnection.connectionstring);
            string faculty = data.faculty.ToLower() + "_query";
            string query = "delete from " + faculty + " where n_id=@n_id and u_id=@u_id and q_time=@q_time";
            using (connection)
            {
                try
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand(query, connection);
                    command.Parameters.AddWithValue("@n_id", data.n_id);
                    command.Parameters.AddWithValue("@u_id", data.u_id);
                    command.Parameters.AddWithValue("@q_time", data.q_time);
                    if (command.ExecuteNonQuery() == 1)
                    {
                        status = true;
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
            return status;
        }
    }
}