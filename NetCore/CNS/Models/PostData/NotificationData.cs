using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CNS.Models.FileHandeling;
using Microsoft.AspNetCore.Http;

namespace CNS.Models.PostData
{
    public class NotificationData
    {
        public NotificationData()
        {
            file = null;
        }


        [Required]
        public string admin_id { get; set; }

        [Required]
        public string admin_password { get; set; }
        public string n_id { get; set; }
        public string n_date { get; set; }
        public string n_time { get; set; }

        [Required]
        [MaxLength(100)]
        public string n_title { get; set; }

        [Required]
        [MaxLength(21800)]
        public string n_message { get; set; }

        public double longitude { get; set; }

        public double latitude { get; set; }

        public List<IFormFile> file { get; set; }

        [Required]
        public string faculty { get; set; }

        [Required]
        public int n_semester { get; set; }

        [Required]
        [Range(0, 1)]
        public int n_event { get; set; }
        public string e_location { get; set; }
        public string e_time { get; set; }
        public int e_participate_status { get; set; }
        public string e_deadline { get; set; }
        public int image_count { get; set; }
        public int file_count { get; set; }
        public int n_edit { get; set; }
        public string[] file_name { get; set; }
        public string[] image_name { get; set; }
        public void ComputeDateTime()
        {
            n_time = System.DateTime.Now.ToString("HH:mm:ss");
            string tmp = "";
            for (int i = 0; i < n_time.Length; i++)
            {
                if (n_time[i] == ':')
                {
                    tmp += '-';
                }
                else
                {
                    tmp += n_time[i];
                }
            }
            n_date = System.DateTime.Now.ToString("yyyy-MM-dd");
            n_id = n_date + "-" + tmp;
        }

        public void CountFileType()
        {
            image_count = 0;
            file_count = 0;
            if (file != null)
            {
                for (int i = 0; i < file.Count; i++)
                {
                    string fileExtension = StoreFile.GetFileExtension(file[i].FileName);
                    if (fileExtension.Equals("jpg") || fileExtension.Equals("jpeg") || fileExtension.Equals("png") || fileExtension.Equals("PNG"))
                    {
                        image_count++;
                    }
                    else
                    {
                        file_count++;
                    }
                }
            }
        }

        public bool EventTypeValidate()
        {
            bool status = true;
            if (n_event == 1)
            {

                if (e_deadline != null && e_location != null && e_time != null && latitude != 0 && longitude != 0)
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
            else
            {
                n_event = 0;
                e_deadline = null;
                e_time = null;
                e_location = null;
            }
            return status;
        }

        public bool NotificationDataValidate()
        {
            bool status = false;

            return status;
        }

        public bool DeleteNotificationValidate()
        {
            bool status = false;
            if (faculty != null && admin_id != null && n_id != null && admin_password != null)
            {
                status = true;
            }
            return status;
        }
    }
}