namespace CNS.Models.PostData
{
    public class QueryData
    {
        public string n_id { get; set; }

        public string u_id { get; set; }

        public string faculty { get; set; }

        public string q_message { get; set; }
        public string q_date { get; set; }

        public string q_time { get; set; }
        public string u_name { get; set; }
    }
}