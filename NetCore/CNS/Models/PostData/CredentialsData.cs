using System.ComponentModel.DataAnnotations;

namespace CNS.Models.PostData
{
    public class CredentialsChangeData
    {
        [Required]
        public string u_id { get; set; }

        [Required]
        public string oldPassword { get; set; }

        [Required]
       // [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,15}$")]
        public string newPassword { get; set; }
    }

    public class ForgotPasswordData
    {
        [Required]
        public string u_id { get; set; }

        [Required]
        [EmailAddress]
        public string email { get; set; }
    }
}