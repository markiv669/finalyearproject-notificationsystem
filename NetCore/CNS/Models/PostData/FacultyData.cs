namespace CNS.Models.PostData
{
    public class FacultyData:StatusModel
    {
        public string f_name { get; set; }
        public string f_address { get; set; }
        public string f_information { get; set; }
        public string f_contact { get; set; }
    }

    public class GetFacultyResponse:StatusModel
    {
        public FacultyData[] faculty { get; set; }
    }
}