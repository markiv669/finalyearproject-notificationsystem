namespace CNS.Models.PostData
{
    public class FcmToken
    {
        public string tokenID { get; set; }
        public string u_id { get; set; }
    }
}