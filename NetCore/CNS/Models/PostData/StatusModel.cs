namespace CNS.Models.PostData
{
    public class StatusModel
    {
        public int code { get; set; }
        public string message { get; set; }
    }
}