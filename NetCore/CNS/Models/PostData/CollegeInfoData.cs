using System.ComponentModel.DataAnnotations;

namespace CNS.Models.PostData
{
    public class CollegeInfoData
    {
        [Required]
        public string c_name { get; set; }
        public string c_image { get; set; }
        public GetFacultyModel[] faculty { get; set; }
    }
}