namespace CNS.Models.PostData
{
    public class AddBatchData
    {
        public string admin_id { get; set; }
        public string password { get; set; }
        public string faculty { get; set; }
        public int batch { get; set; }
    }
}