namespace CNS.Models.PostData
{
    public class QueryReadData
    {
        public string n_id { get; set; }

        public string faculty { get; set; }
        public int offset { get; set; }
    }
}