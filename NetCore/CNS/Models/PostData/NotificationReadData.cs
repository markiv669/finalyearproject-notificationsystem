namespace CNS.Models.PostData
{
    public class NotificationReadData
    {
        public string u_id { get; set; }
        public string faculty { get; set; }
        public int semester { get; set; }
        public int offset { get; set; }
        public int authority { get; set; }

        public string n_id { get; set; }
    }
}