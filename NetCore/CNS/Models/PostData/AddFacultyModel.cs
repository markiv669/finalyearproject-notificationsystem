using System.ComponentModel.DataAnnotations;

namespace CNS.Models.PostData
{
    public class AddFacultyModel
    {
        [Required]
        [StringLength(20)]
        public string faculty_name { get; set; }

        [Required]
        [StringLength(15)]
        public string admin_id { get; set; }

        [Required]
        public string admin_password { get; set; }
        public string f_address { get; set; }
        public string f_information { get; set; }
        public string f_contact { get; set; }
    }
    public class GetFacultyModel
    { 
        public string faculty_name { get; set; }
        public string f_address { get; set; }
        public string f_information { get; set; }
        public string f_contact { get; set; }
    }
}