namespace CNS.Models.PostData
{
    public class GetNotificationResponse: StatusModel
    {
         public NotificationData[] notifications { get; set; }
    }
}