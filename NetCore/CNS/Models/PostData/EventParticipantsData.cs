namespace CNS.Models.PostData
{
    public class EventParticipantsData
    {
        public string n_id { get; set; }
        public string u_id { get; set; }
        public string faculty { get; set; }
    }
}