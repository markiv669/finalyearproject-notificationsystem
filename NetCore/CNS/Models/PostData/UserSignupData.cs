using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace CNS.Models.PostData
{
    public class UserSignupData
    {
        public int code { get; set; } //error code
        public string message { get; set; } //error message

        [Required]
        public string admin_id { get; set; }

        [Required]
        public string admin_password { get; set; }

        [Required]
        [StringLength(15)]
        public string u_id { get; set; }

        [Required]
        [StringLength(50)]
        public string u_fname { get; set; }

        [StringLength(50)]
        public string u_mname { get; set; }

        [Required]
        [StringLength(50)]
        public string u_lname { get; set; }

        [Required]
        public int u_semester { get; set; }

        [Required]
        [StringLength(20)]
        public string u_faculty { get; set; }

        [Required]
        [Phone]
        public string u_phone { get; set; }

        [Required]
        public string father_fname { get; set; }
        public string father_mname { get; set; }

        [Phone]
       
        public string father_phone { get; set; }

        [Required(ErrorMessage="mother name is required")]
        public string mother_fname { get; set; }
        public string mother_mname { get; set; }

        [Phone]
        public string mother_phone { get; set; }

        [Required]
        [EmailAddress]
        public string primary_email { get; set; }

        [EmailAddress]
        public string alternate_email { get; set; }
        public int passwordChangedStatus { get; set; }

        [Required]
        public IFormFile image { get; set; }
        public string profilePicture { get; set; }

        [Required]
        [Range(0, 2)]
        public int authority { get; set; }

        [Required]
        [StringLength(100)]
        public string u_paddress { get; set; }

        [Required]
        [StringLength(100)]
        public string u_taddress { get; set; }

    }
}