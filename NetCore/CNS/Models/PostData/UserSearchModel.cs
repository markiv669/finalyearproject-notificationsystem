using System.Text;

namespace CNS.Models.PostData
{
    public class UserSearchModel
    {
        public string finalQuery { get; set; }
        public string u_semester { get; set; }
        public string u_faculty { get; set; }
        public string u_id { get; set; }
        public string u_mname { get; set; }
        public string u_lname { get; set; }
        public string u_fname { get; set; }

        public string GenerateQuery()
        {
            StringBuilder query = new StringBuilder("select * from users where ");

            if (u_id != null)
            {
                query.Append("u_id='" + u_id + "' && ");
            }
            if (u_faculty != null)
            {
                query.Append("u_faculty='" + u_faculty + "' && ");
            }
            if (u_semester != null)
            {
                query.Append("u_semester=" + u_semester + " && ");
            }
            if (u_fname != null)
            {
                query.Append("u_fname='" + u_fname + "' && ");
            }
            if (u_lname != null)
            {
                query.Append("u_lname='" + u_lname + "' && ");
            }
            if (u_mname != null)
            {
                query.Append("u_mname='" + u_mname + "' && ");
            }

            this.finalQuery = query.ToString();
            this.finalQuery = finalQuery.Substring(0,finalQuery.Length - 3);
            return (finalQuery);
        }

        public bool ValidateData()
        {
            if (u_id == null && u_faculty == null && u_semester == null && u_lname == null && u_fname == null && u_mname == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}