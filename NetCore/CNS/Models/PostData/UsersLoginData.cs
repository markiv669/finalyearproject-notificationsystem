using System.ComponentModel.DataAnnotations;

namespace CNS.Models.PostData
{
    public class UserLoginData
    {
        [Required]
        [StringLength(15)]
        public string u_id { get; set; }

        [Required]
        public string password { get; set; }
    }
}