using System;
using System.Collections.Generic;
using CNS.Models.PostData;
using CNS.Models.PushNotification.Pushy;
using CNS.Models.CRUD;

namespace CNS.Models.PushNotification
{
    public class PushNotification
    {

        public static void NewNotice_SendPushNotification(NotificationData data)
        {
            // Prepare array of target device tokens
            // List<string> deviceTokens = new List<string>();

            // Add your device tokens here
            // deviceTokens.Add("cdd92f4ce847efa5c7f");

            // Convert to string[] array
            // string[] to = deviceTokens.toArray();

            // Optionally, send to a publish/subscribe topic instead

            string to = "/topics/" + data.faculty + data.n_semester;
            to = to.ToLower();


            // Set payload (it can be any object)
            var payload = new Dictionary<string, string>();

            // Add message parameter to dictionary
            payload.Add("message", data.n_message);
            payload.Add("title", data.n_title);

            // iOS notification fields
            var notification = new Dictionary<string, object>();

            notification.Add("badge", 1);
            notification.Add("sound", "ping.aiff");
            notification.Add("body", data.n_message);

            // Prepare the push HTTP request
            PushyPushRequest push = new PushyPushRequest(payload, to, notification);
            System.Console.WriteLine(to);

            try
            {
                // Send the push notification
                PushyAPI.SendPush(push);
            }
            catch (Exception exc)
            {
                // Write error to console
                Console.WriteLine(exc);
            }
        }
        public static void EditNotice_SendPushNotification(EditNotificationData data)
        {
            string to = "/topics/" + data.faculty + data.n_semester;
            to = to.ToLower();


            // Set payload (it can be any object)
            var payload = new Dictionary<string, string>();

            // Add message parameter to dictionary
            payload.Add("message", data.n_title);
            payload.Add("title", "NOTFICATION EDITED");

            // iOS notification fields
            var notification = new Dictionary<string, object>();

            notification.Add("badge", 1);
            notification.Add("sound", "ping.aiff");
            notification.Add("body", data.n_message);
            payload.Add("notificationID", data.n_id);

            // Prepare the push HTTP request
            PushyPushRequest push = new PushyPushRequest(payload, to, notification);
            System.Console.WriteLine(to);

            try
            {
                // Send the push notification
                PushyAPI.SendPush(push);
            }
            catch (Exception exc)
            {
                // Write error to console
                Console.WriteLine(exc);
            }
        }

        public static void Query_SendPushNotification(string faculty, string n_id, string q_message)
        {
            List<string> deviceTokens = AdminTokenRead.Read(faculty);

            for (int i = 0; i < deviceTokens.Count; i++)
            {
                var payload = new Dictionary<string, string>();

                // Add message parameter to dictionary
                payload.Add("message", q_message);
                payload.Add("title", "NEW QUERY");
                payload.Add("notificationID", n_id);

                // iOS notification fields
                var notification = new Dictionary<string, object>();

                notification.Add("badge", 1);
                notification.Add("sound", "ping.aiff");
                notification.Add("body", "You have a new query");

                // Prepare the push HTTP request
                PushyPushRequest push = new PushyPushRequest(payload, deviceTokens[i], notification);
                try
                {
                    // Send the push notification
                    PushyAPI.SendPush(push);
                }
                catch (Exception exc)
                {
                    // Write error to console
                    Console.WriteLine(exc);
                }
            }

        }
    }
}