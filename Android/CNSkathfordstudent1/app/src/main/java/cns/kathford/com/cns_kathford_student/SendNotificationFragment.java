package cns.kathford.com.cns_kathford_student;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import java.net.URISyntaxException;
import java.util.ArrayList;

import java.util.List;


import javax.xml.validation.Validator;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.RetrofitSingleton;
import cns.kathford.com.cns_kathford_student.Models.SendNotificationData;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static android.view.View.VISIBLE;


public class SendNotificationFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    //private SendNotificationData notificationData = new SendNotificationData();
    DatePickerDialog.OnDateSetListener dateSetListener;
    Button button, file_upload;
    Spinner spinner, spinner2;
    EditText n_title, n_message, e_location;
    TextView e_deadline, e_time;
    CheckBox n_event;
    ProgressBar progressBar;
    LinearLayout event_items;
    FloatingActionButton clearButton;

    List<Uri> files = new ArrayList<Uri>();
    SendNotificationData sendNotificationData;
    String semester;

    List<String> fileDeleteList = new ArrayList<String>();
    retrofit2.Call<StatusModel> call;

    public SendNotificationFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sendnotification_recyclerview, container, false);


        n_title = view.findViewById(R.id.n_title);
        n_message = view.findViewById(R.id.n_message);
        e_location = view.findViewById(R.id.e_location);
        e_time = view.findViewById(R.id.e_time);
        e_deadline = view.findViewById(R.id.e_deadline);
        n_event = view.findViewById(R.id.n_event);
        event_items = view.findViewById(R.id.event_list);
        spinner = view.findViewById(R.id.spinner);
        spinner2 = view.findViewById(R.id.spinner2);
        button = view.findViewById(R.id.post_btn);
        progressBar = view.findViewById(R.id.progressBar);
        file_upload = view.findViewById(R.id.select_files);
        setFilesButtonName();
        clearButton = view.findViewById(R.id.clearNotificationPage);

        spinner2.setOnItemSelectedListener(this);
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Everyone");
        categories.add("1 Semester");
        categories.add("2 Semester");
        categories.add("3 Semester");
        categories.add("4 Semester");
        categories.add("5 Semester");
        categories.add("6 Semester");
        categories.add("7 Semester");
        categories.add("8 Semester");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        ArrayAdapter<String> fileDeleteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, fileDeleteList);
        // Drop down layout style
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fileDeleteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner2.setAdapter(fileDeleteAdapter);


        n_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (n_event.isChecked()) {
                    event_items.setVisibility(VISIBLE);
                } else {
                    event_items.setVisibility(View.GONE);
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            String buttonString;

            @Override
            public void onClick(View view) {
                if (button.getText().toString().equals("CANCEL")) {
                    call.cancel();
                    buttonString = "PUBLISH";
                    button.setText(buttonString);
                } else {
                    sendNotificationData = new SendNotificationData();
                    sendNotificationData.setN_semester(semester);
                    sendNotificationData.setN_message(n_message.getText().toString());
                    sendNotificationData.setN_title(n_title.getText().toString());
                    if (n_event.isChecked())
                        sendNotificationData.setN_event("1");
                    else
                        sendNotificationData.setN_event("0");
                    sendNotificationData.setAdmin_id("hari");
                    sendNotificationData.setAdmin_password("H@546694am");
                    sendNotificationData.setE_deadline(e_deadline.getText().toString());
                    sendNotificationData.setE_location(e_location.getText().toString());
                    sendNotificationData.setE_time(e_time.getText().toString());
                    sendNotificationData.setFaculty("BSCCSIT");

                    if (sendNotificationData.Validate()) {
                        buttonString = "CANCEL";
                        button.setText(buttonString);
                        Retrofit retrofit = RetrofitSingleton.getInstance();
                        API api = retrofit.create(API.class);
                        progressBar.setVisibility(VISIBLE);
                        progressBar.animate();
                        MediaType mediaType = MediaType.parse("");
                        MultipartBody.Part[] fileParts = new MultipartBody.Part[files.size()];
                        for (int i = 0; i < files.size(); i++) {
                            try {
                                String path = getFilePath(getContext(), files.get(i));
                                File file = new File(path);
                                RequestBody fileBody = RequestBody.create(mediaType, file);
                                //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                                fileParts[i] = MultipartBody.Part.createFormData("file", file.getName(), fileBody);
                            } catch (Exception ex) {
                                Log.e(TAG, ex.getMessage());
                            }

                        }

                        call = api.SendNotification(sendNotificationData.GetMap(), fileParts);

                        call.enqueue(new Callback<StatusModel>() {
                            @Override
                            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                                progressBar.setVisibility(View.INVISIBLE);

                                if (response.code() == 200) {
                                    StatusModel status = response.body();

                                    Toast.makeText(getContext(), status.getMessage(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getContext(), response.body().toString(), Toast.LENGTH_LONG).show();
                                }
                                buttonString = "PUBLISH";
                                button.setText(buttonString);

                            }

                            @Override
                            public void onFailure(Call<StatusModel> call, Throwable t) {
                                progressBar.setVisibility(View.INVISIBLE);
//                            Toast.makeText(getContext(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
                                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                buttonString = "PUBLISH";
                                button.setText(buttonString);
                            }
                        });
                    } else {
                        Snackbar.make(view, "Invalid Input. Check Again", Snackbar.LENGTH_LONG).show();
                    }
                }

                {

                }

            }

        });

        file_upload.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("*/*");
                // intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,true);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                startActivityForResult(intent, 1);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View view2 = view;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Clear Notification");
                builder.setIcon(R.drawable.kath);
//                builder.setIcon(R.drawable.logout);
                builder.setMessage("Are you sure to clear the notification data?\nEverything will be removed.").setCancelable(true)
                        .setPositiveButton("CLEAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ClearNotificationPage();
                                Snackbar.make(view2, "Page Cleared", Snackbar.LENGTH_LONG).show();
                            }

                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                Button cancelBtn = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                cancelBtn.setTextColor(Color.BLUE);
                cancelBtn.setFocusable(true);
                Button okBtn = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                okBtn.setTextColor(Color.RED);
            }
        });


        n_message.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.n_message) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        n_title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.n_title) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        e_location.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.e_location) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
//
//        e_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        return view;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String path = null;
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                ClipData clipData = data.getClipData();
                if (clipData == null) {
                    Uri uri = data.getData();
                    files.add(uri);
                    try {
                        path = getFilePath(getContext(), uri);
                        if (path != null) {
                            File file = new File(path);
                            fileDeleteList.add(file.getName());
                            file = null;
                        }
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }


                } else {
                    int fileCount = 0;
                    try {
                        fileCount = data.getClipData().getItemCount();
                    } catch (Exception ex) {
                        Log.d("Error FIle:", ex.getMessage());
                    }
                    for (int i = 0; i < fileCount; i++) {
                        Uri uri = data.getClipData().getItemAt(i).getUri();
                        files.add(uri);

                        try {
                            path = getFilePath(getContext(), uri);
                            if (path != null) {
                                File file = new File(path);
                                fileDeleteList.add(file.getName());
                                file = null;
                            }
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }
        setFilesButtonName();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

        int k = parent.getId();
        String item = parent.getItemAtPosition(position).toString();
        switch (item) {
            case "Everyone":
                //notificationData.setN_semester(0);
                semester = "0";
                break;
            case "1 Semester":
                // notificationData.setN_semester(1);
                semester = "1";
                break;
            case "2 Semester":
                semester = "2";
                //notificationData.setN_semester(2);
                break;
            case "3 Semester":
                //notificationData.setN_semester(3);
                semester = "3";
                break;
            case "4 Semester":
                // notificationData.setN_semester(4);
                semester = "4";
                break;
            case "5 Semester":
                //notificationData.setN_semester(5);
                semester = "5";
                break;
            case "6 Semester":
                //notificationData.setN_semester(6);
                semester = "6";
                break;
            case "7 Semester":
                //notificationData.setN_semester(7);
                semester = "7";
                break;
            case "8 Semester":
                //notificationData.setN_semester(8);
                semester = "8";
                break;
            default:
                //notificationData.setN_semester(0);
                semester = "0";
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void ClearNotificationPage() {
        files.clear();
        fileDeleteList.clear();
        n_title.setText("");
        n_message.setText("");
        e_location.setText("");
        e_time.setText("");
        e_deadline.setText("");
        n_event.setChecked(false);
        event_items.setVisibility(View.GONE);
        setFilesButtonName();

    }

    private void setFilesButtonName() {
        String buttonName = "FILES: " + String.valueOf(fileDeleteList.size());
        file_upload.setText(buttonName);
    }

    @SuppressLint({"NewApi", "Recycle"})
    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

}
