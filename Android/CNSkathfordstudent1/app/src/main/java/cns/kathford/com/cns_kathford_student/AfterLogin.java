package cns.kathford.com.cns_kathford_student;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.FcmToken;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import cns.kathford.com.cns_kathford_student.Models.UserSignupData;
import cns.kathford.com.cns_kathford_student.SendPostData.RegistarToken;
import me.pushy.sdk.Pushy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vikru on 6/9/2018.
 */

public class AfterLogin extends Activity {
    UserSignupData userSignupData = LoginActivity.userSignupData;
    private static String registrationStatus;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Pushy.listen(this);
        if (!Pushy.isRegistered(this)) {
            try {
                new AfterLogin.RegisterForPushNotificationsAsync().execute();
            } catch (Exception ex) {
                Log.d("\n\n\nTOKEN FOR PUSHY=", ex.getMessage() + " \n\n\n\n\n");
            }
        } else {
            try {
                String deviceToken = Pushy.register(getApplicationContext());
                String status = RegistarToken.SendToken(deviceToken); //send token to server
            } catch (Exception ex) {

            }
        }
        setContentView(R.layout.activity_main);
        TextView textView = findViewById(R.id.textview);
        String topic = userSignupData.getU_faculty() + userSignupData.getU_semester();
        topic = topic.toLowerCase();
        textView.setText(topic);
    }


    public static String SendToken(String my_token) {
        //send this token to database
        if (my_token != null) {
            FcmToken token = new FcmToken();
            token.setTokenID(my_token);
            token.setU_id(LoginActivity.userSignupData.getU_id());

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.baseURL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            API api = retrofit.create(API.class);

            Call<StatusModel> call = api.SendToken(token);


            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                    //In this point we got our hero list
                    //thats damn easy right ;)
                    StatusModel status = response.body();
                    if (status.getCode() == 0) {
                        registrationStatus = "failed";
                    } else {
                        registrationStatus = "sucess";
                    }

                    //now we can do whatever we want with this list
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    registrationStatus = "failed to reister to server";
                }
            });
        }
        return registrationStatus;
    }

    private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device
                String deviceToken = Pushy.register(getApplicationContext());
//               String status = RegistarToken.SendToken(deviceToken); //send token to server


                String status = RegistarToken.SendToken(deviceToken); //send token to server

                String topic = userSignupData.getU_faculty() + userSignupData.getU_semester();
                topic = topic.toLowerCase();
                Pushy.subscribe(topic, getApplicationContext());
                Pushy.subscribe(userSignupData.getU_faculty().toLowerCase() + "0", getApplicationContext());

            } catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                return;
            }

            // Succeeded, do something to alert the user
        }
    }
}
