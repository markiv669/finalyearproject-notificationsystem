package cns.kathford.com.cns_kathford_student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.ForgotPasswordModel;
import cns.kathford.com.cns_kathford_student.Models.RetrofitSingleton;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import cns.kathford.com.cns_kathford_student.Models.UserLoginData;
import cns.kathford.com.cns_kathford_student.Models.UserSignupData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgotPassword extends AppCompatActivity {

    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";

    EditText email, userID;
    Button submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        email = findViewById(R.id.email);
        userID = findViewById(R.id.user);
        submit = findViewById(R.id.btnSubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordModel forgotPasswordData = new ForgotPasswordModel();
                forgotPasswordData.setU_id(userID.getText().toString());
                forgotPasswordData.setEmail(email.getText().toString());

                Retrofit retrofit = RetrofitSingleton.getInstance();
                API api = retrofit.create(API.class);

                Call<StatusModel> call = api.ForgotPassword(forgotPasswordData.GetMap());


                call.enqueue(new Callback<StatusModel>() {
                    @Override
                    public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                        if (response.code() == 200) {
                            StatusModel status = response.body();

                            Toast.makeText(getApplicationContext(), status.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "cant find the controller/action", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusModel> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }

    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
        startActivity(intent);
    }
}

