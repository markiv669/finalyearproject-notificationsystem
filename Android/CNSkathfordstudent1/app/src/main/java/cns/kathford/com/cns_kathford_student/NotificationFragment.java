package cns.kathford.com.cns_kathford_student;


import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.EndlessRecyclerViewScrollListener;
import cns.kathford.com.cns_kathford_student.Models.NotificationAdapter;
import cns.kathford.com.cns_kathford_student.Models.NotificationReadModel;
import cns.kathford.com.cns_kathford_student.Models.ReceiveNotificationData;
import cns.kathford.com.cns_kathford_student.Models.RetrofitSingleton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationFragment extends Fragment {

    RecyclerView notificationList;
    NotificationAdapter notificationAdapter;
    LinearLayoutManager linearLayoutManager;
    private int offset = 0;

    //variables for pagination >> to load new notifications as scrolling down the page

    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previous_total = 0;
    private int view_threshold = 10;

    public NotificationFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_recyclerview, container, false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        notificationList = view.findViewById(R.id.notice_rv);

        notificationList.setLayoutManager(linearLayoutManager);

        notificationList.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Toast.makeText(getActivity(), "LAst", Toast.LENGTH_LONG).show();
                        offset+=10;
                        PerformPagination();
                        isLoading = true;
            }
        });

//        commentList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                visibleItemCount = linearLayoutManager.getChildCount();
//                totalItemCount = linearLayoutManager.getItemCount();
//                pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
//
//                if (dy > 0) {
//                    if (isLoading) {
//                        if (totalItemCount > previous_total) {
//                            isLoading = false;
//                            previous_total = totalItemCount;
//                        }
//                    }
//                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVisibleItems + view_threshold)) {
//                        offset += 10;
//                        PerformPagination();
//                        isLoading = true;
//                    }
//                }
//            }
//        });


        Retrofit retrofit = RetrofitSingleton.getInstance();
        API api = retrofit.create(API.class);
        NotificationReadModel notificationReadModel = new NotificationReadModel();
        notificationReadModel.setFaculty("BSCCSIT");
        notificationReadModel.setSemester("7");
        notificationReadModel.setOffset(String.valueOf(offset));
        Call<List<ReceiveNotificationData>> call = api.GetNotification(notificationReadModel.GetMap());

        call.enqueue(new Callback<List<ReceiveNotificationData>>() {
            @Override
            public void onResponse(Call<List<ReceiveNotificationData>> call, Response<List<ReceiveNotificationData>> response) {

                if (response.code() == 200) {
                    List<ReceiveNotificationData> data = response.body();
                    if (data != null) {
                        if (data.get(0).getCode() != null) {
                            Toast.makeText(getContext(), data.get(0).getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            notificationAdapter = new NotificationAdapter(data, getActivity());
                            notificationList.setAdapter(notificationAdapter);
                        }
                    } else {
                        Toast.makeText(getContext(), "no data......", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), response.body().toString(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<List<ReceiveNotificationData>> call, Throwable t) {
//                            Toast.makeText(getContext(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        return view;
    }


    private void PerformPagination() {
        Retrofit retrofit = RetrofitSingleton.getInstance();
        API api = retrofit.create(API.class);
        NotificationReadModel notificationReadModel = new NotificationReadModel();
        notificationReadModel.setFaculty("BSCCSIT");
        notificationReadModel.setSemester("7");
        notificationReadModel.setOffset(String.valueOf(offset));
        Call<List<ReceiveNotificationData>> call = api.GetNotification(notificationReadModel.GetMap());

        call.enqueue(new Callback<List<ReceiveNotificationData>>() {
            @Override
            public void onResponse(Call<List<ReceiveNotificationData>> call, Response<List<ReceiveNotificationData>> response) {

                if (response.code() == 200) {
                    List<ReceiveNotificationData> data = response.body();
                    if (data != null) {
                        if (data.get(0).getCode() != null) {
                            Toast.makeText(getContext(), data.get(0).getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            notificationAdapter.AddNotifications(data);
                        }
                    } else {
                        Log.e("error", "no data to fetch");
                    }

                } else {
                    Toast.makeText(getContext(), response.body().toString(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<List<ReceiveNotificationData>> call, Throwable t) {
//                            Toast.makeText(getContext(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


}
