package cns.kathford.com.cns_kathford_student;

import android.content.Context;
import android.content.SharedPreferences;

public class CNSpreference {
    private static final String PREFS_NAME = "CNS_prefs";

    private static final String USER_NAME = "_user_name";
    private static final String USER_PASSWORD = "_user_password";

    private static final String USER_MNAME = "_user_mname";
    private static final String USER_LNAME = "_user_lname";
    private static final String USER_ADDRESS = "_user_address";
    private static final String USER_ID = "_user_id";

    private static final String USER_SEMESTER = "_user_semester";
    private static final String USER_FACULTY = "_user_faculty";
    private static final String USER_PHONE = "_user_phone";
    private static final String FATHER_FNAME = "_father_fname";
    private static final String FATHER_MNAME = "_father_mname";
    private static final String MOTHER_FNAME = "_mother_fname";
    private static final String MOTHER_MNAME = "_mother_mname";
    private static final String MOTHER_PHONE = "_mother_phone";
    private static final String USER_EMAIL = "_user_email";
    private static final String FATHER_PHONE = "_father_phone";
    private static final String PROFILE_PICTURE = "_profile_picture";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    public CNSpreference(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public String getUserName() {
        return mSharedPreferences.getString(USER_NAME, null);
    }

    public  String getUserPassword() {
        return mSharedPreferences.getString(USER_PASSWORD, null);
    }

    public void setUserPassword(String password){
        mEditor = mSharedPreferences.edit();

        mEditor.putString(USER_PASSWORD, password);
        mEditor.apply();
    }
    public void setUserName(String name) {
        mEditor = mSharedPreferences.edit();

        mEditor.putString(USER_NAME, name);
        mEditor.apply();
    }

    public String getPrrofilePicture() {
        return mSharedPreferences.getString(PROFILE_PICTURE, null);
    }

    public void setProfilePicture(String name) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(PROFILE_PICTURE, name);
        mEditor.apply();
    }

    public String getUserAddress() {
        return mSharedPreferences.getString(USER_ADDRESS, null);
    }

    public void setUserAddress(String userAddress) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_ADDRESS, userAddress);
        mEditor.apply();
    }

    public String getUserId() {
        return mSharedPreferences.getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_ID, userId);
        mEditor.apply();
    }

    public String getUserMname() {
        return mSharedPreferences.getString(USER_MNAME, null);
    }

    public void setUserMname(String mname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_MNAME, mname);
        mEditor.apply();
    }

    public String getUserLname() {
        return mSharedPreferences.getString(USER_LNAME, null);
    }

    public void setUserLname(String lname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_LNAME, lname);
        mEditor.apply();
    }

    public String getUserSemester() {
        return mSharedPreferences.getString(USER_SEMESTER, null);
    }

    public void setUserSemester(String semester) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_SEMESTER, semester);
        mEditor.apply();
    }

    public String getUserFaculty() {
        return mSharedPreferences.getString(USER_FACULTY, null);
    }

    public void setUserFaculty(String faculty) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_FACULTY, faculty);
        mEditor.apply();
    }

    public String getUserPhone() {
        return mSharedPreferences.getString(USER_PHONE, null);
    }

    public void setUserPhone(String phone) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_PHONE, phone);
        mEditor.apply();
    }

    public String getFatherFname() {
        return mSharedPreferences.getString(FATHER_FNAME, null);
    }

    public void setFatherFname(String fatherFname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(FATHER_FNAME, fatherFname);
        mEditor.apply();
    }

    public String getFatherMname() {
        return mSharedPreferences.getString(FATHER_MNAME, null);
    }

    public void setFatherMname(String fatherMname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(FATHER_MNAME, fatherMname);
        mEditor.apply();
    }

    public String getMotherFname() {
        return mSharedPreferences.getString(MOTHER_FNAME, null);
    }

    public void setMotherFname(String motherFname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_MNAME, motherFname);
        mEditor.apply();
    }

    public String getMotherMname() {
        return mSharedPreferences.getString(MOTHER_FNAME, null);
    }

    public void setMotherMname(String motherMname) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(MOTHER_MNAME, motherMname);
        mEditor.apply();
    }

    public String getMotherPhone() {
        return mSharedPreferences.getString(MOTHER_PHONE, null);
    }

    public void setMotherPhone(String motherPhone) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(MOTHER_PHONE, motherPhone);
        mEditor.apply();
    }

    public String getUserEmail() {
        return mSharedPreferences.getString(USER_EMAIL, null);
    }

    public void setUserEmail(String userEmail) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(USER_EMAIL, userEmail);
        mEditor.apply();
    }

    public String getFatherPhone() {
        return mSharedPreferences.getString(FATHER_PHONE, null);
    }

    public void setFatherPhone(String fatherPhone) {
        mEditor = mSharedPreferences.edit();
        mEditor.putString(FATHER_PHONE, fatherPhone);
        mEditor.apply();
    }

    public void clearData() {
        mEditor = mSharedPreferences.edit();
        mEditor.clear();
        mEditor.apply();
    }


}
