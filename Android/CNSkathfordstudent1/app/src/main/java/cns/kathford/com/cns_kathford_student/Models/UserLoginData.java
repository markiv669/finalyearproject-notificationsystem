package cns.kathford.com.cns_kathford_student.Models;

import java.util.HashMap;

import okhttp3.RequestBody;

public class UserLoginData {
    private String u_id;
    private String password;

    public String getUserName() {
        return u_id;
    }

    public void setUserName(String u_id) {
        this.u_id = u_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HashMap<String, RequestBody> GetMap() {
        RequestBody u_id = RequestBody.create(okhttp3.MultipartBody.FORM, getUserName());
        RequestBody password = RequestBody.create(okhttp3.MultipartBody.FORM, getPassword());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("u_id", u_id);
        map.put("password", password);
        return map;
    }
}
