package cns.kathford.com.cns_kathford_student.Models;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by vikru on 7/9/2018.
 */

public class NotificationReadModel {
    String semester;
    String faculty;
    String offset;

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }


    public HashMap<String, RequestBody> GetMap() {

        RequestBody semester = RequestBody.create(okhttp3.MultipartBody.FORM, getSemester());
        RequestBody faculty = RequestBody.create(okhttp3.MultipartBody.FORM, getFaculty());
        RequestBody offset = RequestBody.create(okhttp3.MultipartBody.FORM, getOffset());

        HashMap<String, RequestBody> map = new HashMap<>();

        map.put("semester", semester);
        map.put("faculty", faculty);
        map.put("offset", offset);
        return map;
    }
}
