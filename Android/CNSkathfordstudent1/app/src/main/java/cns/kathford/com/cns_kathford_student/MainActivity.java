package cns.kathford.com.cns_kathford_student;


import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import me.pushy.sdk.Pushy;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Pushy.listen(this);
        if(!Pushy.isRegistered(this)) {
            try {
                new RegisterForPushNotificationsAsync().execute();
            } catch (Exception ex) {
                Log.d("\n\n\nTOKEN FOR PUSHY=", ex.getMessage() + " \n\n\n\n\n");
            }
        }
        setContentView(R.layout.activity_main);
    }


    private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device
                String deviceToken = Pushy.register(getApplicationContext());
                // String status = RegistarToken.SendToken(deviceToken);
                Pushy.subscribe("bsccsit7", getApplicationContext());

            } catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                return;
            }

            // Succeeded, do something to alert the user
        }
    }
}


