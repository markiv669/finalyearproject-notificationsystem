package cns.kathford.com.cns_kathford_student;


import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.ChangePasswordModel;
import cns.kathford.com.cns_kathford_student.Models.ForgotPasswordModel;
import cns.kathford.com.cns_kathford_student.Models.RetrofitSingleton;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import cns.kathford.com.cns_kathford_student.Models.UserLoginData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChangePasswordFragment extends Fragment {

    EditText newPassword;
    EditText oldPassword;
    EditText rePassword;
    Button changePassword;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_changepassword, null);
        oldPassword = view.findViewById(R.id.old_pass);

        newPassword = view.findViewById(R.id.new_pass);

        rePassword = view.findViewById(R.id.re_new_pass);

        changePassword = view.findViewById(R.id.btnChange);

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CheckValidity()) {
                    ChangePasswordModel changePasswordModel = new ChangePasswordModel();
                    changePasswordModel.setNewPassword(newPassword.getText().toString());
                    changePasswordModel.setOldPassword(oldPassword.getText().toString());
                    changePasswordModel.setU_id(LoginActivity.userSignupData.getU_id());

                    Retrofit retrofit = RetrofitSingleton.getInstance();
                    API api = retrofit.create(API.class);

                    Call<StatusModel> call = api.ChangePassword(changePasswordModel.GetMap());


                    call.enqueue(new Callback<StatusModel>() {
                        @Override
                        public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                            if (response.code() == 200) {
                                StatusModel status = response.body();
                                oldPassword.setText("");
                                newPassword.setText("");
                                rePassword.setText("");
                                Toast.makeText(getActivity(), status.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "cant find the controller/action", Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<StatusModel> call, Throwable t) {
                            oldPassword.setText("");
                            newPassword.setText("");
                            rePassword.setText("");
                            Toast.makeText(getActivity(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        return view;
    }

    private boolean CheckValidity() {
        boolean status = true;
        if (oldPassword.getText().toString().equalsIgnoreCase("")) {
            oldPassword.setHint("Enter current Password");
            status = false;
        }
        if (newPassword.getText().toString().equals(rePassword.getText().toString())) {
            if (newPassword.getText().toString().equalsIgnoreCase("")) {
                newPassword.setHint("Enter new Password");
                status = false;
            }
            if (rePassword.getText().toString().equalsIgnoreCase("")) {
                rePassword.setHint("Enter new Password");
                status = false;
            }
            if (oldPassword.getText().toString().equals(newPassword.getText().toString())) {
                Toast.makeText(getActivity(), "Old password and New Password are same. Choose Some Other New Password", Toast.LENGTH_SHORT).show();
                status = false;
            }
        } else {
            Toast.makeText(getActivity(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            status = false;
        }
        return status;
    }
}
