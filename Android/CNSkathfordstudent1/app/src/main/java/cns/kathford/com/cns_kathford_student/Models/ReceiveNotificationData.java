package cns.kathford.com.cns_kathford_student.Models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiveNotificationData {

    @SerializedName("code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("admin_id")
    @Expose
    private Object adminId;
    @SerializedName("admin_password")
    @Expose
    private Object adminPassword;
    @SerializedName("n_id")
    @Expose
    private String nId;
    @SerializedName("n_date")
    @Expose
    private String nDate;
    @SerializedName("n_time")
    @Expose
    private String nTime;
    @SerializedName("n_title")
    @Expose
    private String nTitle;
    @SerializedName("n_message")
    @Expose
    private String nMessage;
    @SerializedName("file")
    @Expose
    private Object file;
    @SerializedName("faculty")
    @Expose
    private Object faculty;
    @SerializedName("n_semester")
    @Expose
    private Integer nSemester;
    @SerializedName("n_event")
    @Expose
    private Integer nEvent;
    @SerializedName("e_location")
    @Expose
    private String eLocation;
    @SerializedName("e_time")
    @Expose
    private String eTime;
    @SerializedName("e_deadline")
    @Expose
    private String eDeadline;
    @SerializedName("image_count")
    @Expose
    private Integer imageCount;
    @SerializedName("file_count")
    @Expose
    private Integer fileCount;
    @SerializedName("file_name")
    @Expose
    private List<String> fileName = null;
    @SerializedName("image_name")
    @Expose
    private List<String> imageName;

    public Object getAdminId() {
        return adminId;
    }

    public void setAdminId(Object adminId) {
        this.adminId = adminId;
    }

    public Object getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(Object adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getNId() {
        return nId;
    }

    public void setNId(String nId) {
        this.nId = nId;
    }

    public String getNDate() {
        return nDate;
    }

    public void setNDate(String nDate) {
        this.nDate = nDate;
    }

    public String getNTime() {
        return nTime;
    }

    public void setNTime(String nTime) {
        this.nTime = nTime;
    }

    public String getNTitle() {
        return nTitle;
    }

    public void setNTitle(String nTitle) {
        this.nTitle = nTitle;
    }

    public String getNMessage() {
        return nMessage;
    }

    public void setNMessage(String nMessage) {
        this.nMessage = nMessage;
    }

    public Object getFile() {
        return file;
    }

    public void setFile(Object file) {
        this.file = file;
    }

    public Object getFaculty() {
        return faculty;
    }

    public void setFaculty(Object faculty) {
        this.faculty = faculty;
    }

    public Integer getNSemester() {
        return nSemester;
    }

    public void setNSemester(Integer nSemester) {
        this.nSemester = nSemester;
    }

    public Integer getNEvent() {
        return nEvent;
    }

    public void setNEvent(Integer nEvent) {
        this.nEvent = nEvent;
    }

    public String getELocation() {
        return eLocation;
    }

    public void setELocation(String eLocation) {
        this.eLocation = eLocation;
    }

    public String getETime() {
        return eTime;
    }

    public void setETime(String eTime) {
        this.eTime = eTime;
    }

    public String getEDeadline() {
        return eDeadline;
    }

    public void setEDeadline(String eDeadline) {
        this.eDeadline = eDeadline;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public void setImageCount(Integer imageCount) {
        this.imageCount = imageCount;
    }

    public Integer getFileCount() {
        return fileCount;
    }

    public void setFileCount(Integer fileCount) {
        this.fileCount = fileCount;
    }

    public List<String> getFileName() {
        return fileName;
    }

    public void setFileName(List<String> fileName) {
        this.fileName = fileName;
    }

    public List<String> getImageName() {
        return imageName;
    }

    public void setImageName(List<String> imageName) {
        this.imageName = imageName;
    }

}
