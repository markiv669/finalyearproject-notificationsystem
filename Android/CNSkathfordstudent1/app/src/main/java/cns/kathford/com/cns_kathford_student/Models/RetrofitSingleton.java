package cns.kathford.com.cns_kathford_student.Models;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vikru on 6/9/2018.
 */

public class RetrofitSingleton {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API.baseURL)
            .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
            .build();

    public static Retrofit getInstance() {
        return retrofit;
    }
}
