package cns.kathford.com.cns_kathford_student.Models;

/**
 * Created by vikru on 6/7/2018.
 */

public class StatusModel {
    private int code ;
    public String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
