package cns.kathford.com.cns_kathford_student.Models;

import android.content.Context;
import android.view.View;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by vikru on 6/30/2018.
 */

public class SendNotificationData {
    private String admin_id;
    private String admin_password;
    private String n_message;
    private String n_title;
    private String n_semester;
    private String n_event;
    private String e_location;
    private String e_deadline;
    private String e_time;
    private String faculty;

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setN_semester(String n_semester) {
        this.n_semester = n_semester;
    }

    public String getN_semester() {
        return n_semester;
    }

    public String getN_event() {
        return n_event;
    }

    public void setN_event(String n_event) {
        this.n_event = n_event;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getAdmin_password() {
        return admin_password;
    }

    public void setAdmin_password(String admin_password) {
        this.admin_password = admin_password;
    }


    public String getE_location() {
        return e_location;
    }

    public void setE_location(String e_location) {
        this.e_location = e_location;
    }

    public String getE_deadline() {
        return e_deadline;
    }

    public void setE_deadline(String e_deadline) {
        this.e_deadline = e_deadline;
    }

    public String getE_time() {
        return e_time;
    }

    public void setE_time(String e_time) {
        this.e_time = e_time;
    }


    public String getN_message() {
        return n_message;
    }

    public void setN_message(String n_message) {
        this.n_message = n_message;
    }

    public String getN_title() {
        return n_title;
    }

    public void setN_title(String n_title) {
        this.n_title = n_title;
    }

    public HashMap<String, RequestBody> GetMap() {
        RequestBody admin_id = RequestBody.create(okhttp3.MultipartBody.FORM, getAdmin_id());
        RequestBody admin_password = RequestBody.create(okhttp3.MultipartBody.FORM, getAdmin_password());
        RequestBody n_title = RequestBody.create(okhttp3.MultipartBody.FORM, getN_title());
        RequestBody n_message = RequestBody.create(okhttp3.MultipartBody.FORM, getN_message());
        RequestBody n_semester = RequestBody.create(MultipartBody.FORM, getN_semester());
        RequestBody n_event = RequestBody.create(okhttp3.MultipartBody.FORM, getN_event());
        RequestBody e_deadline = RequestBody.create(okhttp3.MultipartBody.FORM, getE_deadline());
        RequestBody e_time = RequestBody.create(okhttp3.MultipartBody.FORM, getE_deadline());
        RequestBody e_location = RequestBody.create(okhttp3.MultipartBody.FORM, getE_deadline());
        RequestBody faculty = RequestBody.create(okhttp3.MultipartBody.FORM, getFaculty());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("n_title", n_title);
        map.put("n_message", n_message);
        map.put("n_event", n_event);
        map.put("admin_id", admin_id);
        map.put("admin_password", admin_password);
        map.put("e_deadline", e_deadline);
        map.put("e_time", e_time);
        map.put("e_location", e_location);
        map.put("faculty", faculty);
        map.put("n_semester", n_semester);
        return map;
    }

    public Boolean Validate() {
        Boolean status = true;

        if (getN_message().equals("") || getN_message().length() > 21800)
            status = false;
        if (getN_title().equals("") || getN_title().length() > 90)
            status = false;
        if (getN_event().equals("1")) {
            if (getE_deadline().equals(""))
                status = false;
            if (getE_time().equals(""))
                status = false;
            if (getE_location().equals(""))
                status = false;
        }
        return status;
    }
}
