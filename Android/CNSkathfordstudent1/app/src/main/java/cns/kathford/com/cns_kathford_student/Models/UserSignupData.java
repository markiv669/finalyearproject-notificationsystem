package cns.kathford.com.cns_kathford_student.Models;

public class UserSignupData {
    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    private String profilePicture;
    private String u_id;
    private String u_fname;
    private String u_mname;
    private String u_lname;
    private String u_paddress;
    private String u_semester;
    private String u_faculty;
    private String u_phone;
    private String father_fname;
    private String father_mname;
    private String mother_fname;
    private String mother_mname;
    private String mother_phone;
    private String father_phone;
    private String primary_email;
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getU_fname() {
        return u_fname;
    }

    public void setU_fname(String u_fname) {
        this.u_fname = u_fname;
    }

    public String getU_mname() {
        return u_mname;
    }

    public void setU_mname(String u_mname) {
        this.u_mname = u_mname;
    }

    public String getU_lname() {
        return u_lname;
    }

    public void setU_lname(String u_lname) {
        this.u_lname = u_lname;
    }

    public String getU_paddress() {
        return u_paddress;
    }

    public void setU_paddress(String u_paddress) {
        this.u_paddress = u_paddress;
    }

    public String getU_semester() {
        return u_semester;
    }

    public void setU_semester(String u_semester) {
        this.u_semester = u_semester;
    }

    public String getU_faculty() {
        return u_faculty;
    }

    public void setU_faculty(String u_faculty) {
        this.u_faculty = u_faculty;
    }

    public String getU_phone() {
        return u_phone;
    }

    public void setU_phone(String u_phone) {
        this.u_phone = u_phone;
    }

    public String getFather_fname() {
        return father_fname;
    }

    public void setFather_fname(String father_fname) {
        this.father_fname = father_fname;
    }

    public String getFather_mname() {
        return father_mname;
    }

    public void setFather_mname(String father_mname) {
        this.father_mname = father_mname;
    }

    public String getMother_fname() {
        return mother_fname;
    }

    public void setMother_fname(String mother_fname) {
        this.mother_fname = mother_fname;
    }

    public String getMother_mname() {
        return mother_mname;
    }

    public void setMother_mname(String mother_mname) {
        this.mother_mname = mother_mname;
    }

    public String getMother_phone() {
        return mother_phone;
    }

    public void setMother_phone(String mother_phone) {
        this.mother_phone = mother_phone;
    }

    public String getFather_phone() {
        return father_phone;
    }

    public void setFather_phone(String father_phone) {
        this.father_phone = father_phone;
    }

    public String getPrimary_email() {
        return primary_email;
    }

    public void setPrimary_email(String primary_email) {
        this.primary_email = primary_email;
    }

    public String getPasswordChangedStatus() {
        return passwordChangedStatus;
    }

    public void setPasswordChangedStatus(String passwordChangedStatus) {
        this.passwordChangedStatus = passwordChangedStatus;
    }

    private String passwordChangedStatus;
}
