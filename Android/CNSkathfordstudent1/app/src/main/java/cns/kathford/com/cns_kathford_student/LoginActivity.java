package cns.kathford.com.cns_kathford_student;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.RetrofitSingleton;
import cns.kathford.com.cns_kathford_student.Models.UserLoginData;
import cns.kathford.com.cns_kathford_student.Models.UserSignupData;
import me.pushy.sdk.Pushy;
import me.pushy.sdk.lib.jackson.databind.ObjectMapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class LoginActivity extends Activity {

    static UserSignupData userSignupData = new UserSignupData();
    boolean status = false;

    RelativeLayout rellay1, rellay2;
    EditText userID;
    EditText password;
    Button login;
    Button forgot_password;

    private static String KEY_SUCCESS = "success";
    private static String KEY_UID = "uid";

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Check for saved logins and if valid switches to dashboard immediately
        CheckSavedCredentials();

        setContentView(R.layout.activity_login);
        //  setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        rellay1 = findViewById(R.id.rellay1);
        rellay2 = findViewById(R.id.rellay2);

        handler.postDelayed(runnable, 3000); //3000 is the timeout for the splash
        userID = findViewById(R.id.user);
        password = findViewById(R.id.pass);
        login = findViewById(R.id.btnLogin);
        forgot_password = findViewById(R.id.btnForgot);

        //IF USER TRIES TO LOG IN WITH CREDENTIALS
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserLoginData userLoginData = new UserLoginData();
                userLoginData.setUserName(userID.getText().toString());
                userLoginData.setPassword(password.getText().toString());
                CheckLogin(userLoginData);
            }
        });

        //IF USER FORGETS PASSWORD
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ForgotPassword.class);
                startActivityForResult(intent, 0);
            }
        });
    }


    private void CheckLogin(final UserLoginData userLoginData) {


        Retrofit retrofit = RetrofitSingleton.getInstance();
        API api = retrofit.create(API.class);

        Call<UserSignupData> call = api.CheckLogin(userLoginData.GetMap());

        call.enqueue(new Callback<UserSignupData>() {
            @Override
            public void onResponse(Call<UserSignupData> call, Response<UserSignupData> response) {
                if (response.code() == 200) {
                    userSignupData = response.body();
                    if (userSignupData.getU_id() != null) {
                        CNSpreference prefs = new CNSpreference(LoginActivity.this);
                        prefs.setFatherFname(userSignupData.getFather_fname());
                        prefs.setFatherMname(userSignupData.getFather_mname());
                        prefs.setFatherPhone(userSignupData.getFather_phone());
                        prefs.setMotherFname(userSignupData.getMother_fname());
                        prefs.setMotherMname(userSignupData.getMother_mname());
                        prefs.setMotherPhone(userSignupData.getMother_phone());
                        prefs.setUserEmail(userSignupData.getPrimary_email());
                        prefs.setUserName(userSignupData.getU_fname());
                        prefs.setUserMname(userSignupData.getU_mname());
                        prefs.setUserLname(userSignupData.getU_lname());
                        prefs.setUserAddress(userSignupData.getU_paddress());
                        prefs.setUserId(userSignupData.getU_id());
                        prefs.setUserPhone(userSignupData.getU_phone());
                        prefs.setProfilePicture(userSignupData.getProfilePicture());
                        prefs.setUserFaculty(userSignupData.getU_faculty());
                        prefs.setUserSemester(userSignupData.getU_semester());
                        //Encrypt password before storing it in sharedpreferences
                        prefs.setUserPassword(Crypto.EncryptAES(userLoginData.getPassword()));

                        //switch to users dashboard
                        Intent intent = new Intent(LoginActivity.this, Navigation.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), userSignupData.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "cant find the controller/action", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserSignupData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error: Check Internet Connection or Contact System Admin", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void CheckSavedCredentials() {
        //read SD card
        //if credentials present in sd card send them to server for verification
        //if valid set pushySubscriptionStatus to true
        //switch to another activity

        CNSpreference prefs = new CNSpreference(this);
        String data=prefs.getUserFaculty();
        if (data!= null) {
            Intent intent = new Intent(LoginActivity.this, Navigation.class);
            startActivity(intent);
            finish();
        }

    }

}








