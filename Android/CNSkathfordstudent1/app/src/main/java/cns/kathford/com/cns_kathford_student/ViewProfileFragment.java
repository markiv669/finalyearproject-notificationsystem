package cns.kathford.com.cns_kathford_student;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import cns.kathford.com.cns_kathford_student.Models.API;

public class ViewProfileFragment extends Fragment {

    TextView tv_name;
    TextView tv_id;
    TextView tv_email;
    TextView tv_phone;
    TextView tv_address;
    TextView tv_fname;
    TextView tv_mname;
    TextView tv_fphone;
    TextView tv_mphone;
    ImageView profilePicture;
    private CNSpreference mPrefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewprofile, container, false);
        tv_name = view.findViewById(R.id.tv_name);
        tv_id = view.findViewById(R.id.tv_id);
        tv_email = view.findViewById(R.id.tv_email);
        tv_phone = view.findViewById(R.id.tv_phone);
        tv_address = view.findViewById(R.id.tv_address);
        tv_fname = view.findViewById(R.id.tv_fname);
        tv_fphone = view.findViewById(R.id.tv_fphone);
        tv_mname = view.findViewById(R.id.tv_mname);
        tv_mphone = view.findViewById(R.id.tv_mphone);
        profilePicture = view.findViewById(R.id.imgUser);

        mPrefs = new CNSpreference(inflater.getContext());
        Glide.with(this)
                .load(API.baseURL + "/file/profilepicture?name=" + mPrefs.getPrrofilePicture())
                .into(profilePicture);


        StringBuilder builder = new StringBuilder();
        builder.append(mPrefs.getUserName());
        builder.append(" ");
        if (mPrefs.getUserMname() != null) {
            builder.append(mPrefs.getUserMname());
            builder.append(" ");
        }
        builder.append(mPrefs.getUserLname());
        tv_name.setText(builder.toString());

        tv_email.setText(mPrefs.getUserEmail());
        tv_phone.setText(mPrefs.getUserPhone());
        tv_id.setText(mPrefs.getUserId());
        tv_address.setText(mPrefs.getUserAddress());
        builder = new StringBuilder();
        builder.append(mPrefs.getFatherFname());
        builder.append(" ");
        if (mPrefs.getFatherMname() != null) {
            builder.append(mPrefs.getFatherMname());
            builder.append(" ");
        }
        builder.append(mPrefs.getUserLname());
        tv_fname.setText(builder.toString());

        tv_fphone.setText(mPrefs.getFatherPhone());

        builder=new StringBuilder();
        builder.append(mPrefs.getMotherFname());
        builder.append(" ");
        if (mPrefs.getMotherMname() != null) {
            builder.append(mPrefs.getMotherMname());
            builder.append(" ");
        }
        builder.append(mPrefs.getUserLname());
        tv_mname.setText(builder.toString());

        tv_mphone.setText(mPrefs.getMotherPhone());

        StringBuilder fullname = new StringBuilder();
        fullname.append(mPrefs.getUserName());
        if (mPrefs.getUserMname() != null) {
            fullname.append(" ");
            fullname.append(mPrefs.getUserMname());
        }
        fullname.append(" ");
        fullname.append(mPrefs.getUserLname());
        tv_id.setText(mPrefs.getUserId());
        tv_phone.setText(mPrefs.getUserPhone());
        tv_email.setText(mPrefs.getUserEmail());
        tv_name.setText(fullname.toString().toUpperCase());
        tv_address.setText(mPrefs.getUserAddress().toUpperCase());

        fullname = new StringBuilder();
        fullname.append(mPrefs.getFatherFname());
        if (mPrefs.getFatherMname() != null) {
            fullname.append(" ");
            fullname.append(mPrefs.getFatherMname());
        }
        fullname.append(" ");
        fullname.append(mPrefs.getUserLname().toUpperCase());
        tv_fname.setText(fullname.toString().toUpperCase());
        tv_fphone.setText(mPrefs.getFatherPhone());

        fullname = new StringBuilder();
        fullname.append(mPrefs.getMotherFname());
        if (mPrefs.getMotherMname() != null) {
            fullname.append(" ");
            fullname.append(mPrefs.getMotherMname());
        }
        fullname.append(" ");
        fullname.append(mPrefs.getUserLname());
        tv_mname.setText(fullname.toString().toUpperCase());
        tv_mphone.setText(mPrefs.getMotherPhone());


        return view;
    }

}
