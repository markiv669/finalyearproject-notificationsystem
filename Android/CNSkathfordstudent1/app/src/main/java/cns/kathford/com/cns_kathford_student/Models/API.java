package cns.kathford.com.cns_kathford_student.Models;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface API {

    //String baseURL = "http://104.211.153.110:5000";
    String baseURL = "http://192.168.100.68:5000/";


    @POST("UserActivity/RegisterDeviceToken")
    Call<StatusModel> SendToken(@Body FcmToken data);


//    @POST("UserActivity/login")
//    Call<UserSignupData> CheckLogin(@Body UserLoginData data);

    @Multipart
    @POST("UserActivity/login")
    Call<UserSignupData> CheckLogin(@PartMap() Map<String, RequestBody> data);

    @Multipart
    @POST("UserActivity/ForgotPassword")
    Call<StatusModel> ForgotPassword(@PartMap() Map<String, RequestBody> data);

    @Multipart
    @POST("UserActivity/ChangeCredential")
    Call<StatusModel> ChangePassword(@PartMap() Map<String, RequestBody> data);

    @Multipart
    @POST("notification/SendNotification")
    Call<StatusModel> SendNotification(@PartMap() Map<String, RequestBody> data,
                                       @Part MultipartBody.Part[] files);


    @Multipart
    @POST("notification/GetNotification")
    Call<List<ReceiveNotificationData>> GetNotification(@PartMap() Map<String, RequestBody> data);

    @Multipart
    @POST("useractivity/readquery")
    Call<List<ReceiveQueryData>> ReadQuery(@PartMap() Map<String, RequestBody> data);
//    @POST("UserActivity/notification/getnotification")
//    Call<NotificationModel>  getnotification(@Body ReadNotificationModel data);
}
