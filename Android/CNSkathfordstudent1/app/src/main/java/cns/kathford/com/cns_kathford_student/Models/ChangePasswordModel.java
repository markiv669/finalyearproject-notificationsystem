package cns.kathford.com.cns_kathford_student.Models;

import java.util.HashMap;

import okhttp3.RequestBody;

/**
 * Created by vikru on 6/23/2018.
 */

public class ChangePasswordModel {
    private String oldPassword;
    private String newPassword;
    private String u_id;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public HashMap<String, RequestBody> GetMap() {
        RequestBody u_id = RequestBody.create(okhttp3.MultipartBody.FORM, getU_id());
        RequestBody oldPassword = RequestBody.create(okhttp3.MultipartBody.FORM, getOldPassword());
        RequestBody newPassword = RequestBody.create(okhttp3.MultipartBody.FORM, getNewPassword());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("u_id", u_id);
        map.put("oldPassword", oldPassword);
        map.put("newPassword", newPassword);
        return map;
    }
}
