package cns.kathford.com.cns_kathford_student.Models;

public class SendQueryModel {
    String n_id;
    String u_id;
    String faculty;
    String q_message;

    public String getN_id() {        return n_id; }

    public void setN_id(String n_id) {
        this.n_id = n_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getQ_message() {
        return q_message;
    }

    public void setQ_message(String q_message) {
        this.q_message = q_message;
    }
}
