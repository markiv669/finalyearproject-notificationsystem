package cns.kathford.com.cns_kathford_student;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cns.kathford.com.cns_kathford_student.Models.ReceiveQueryData;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private List<ReceiveQueryData> query;
    private Context context;

    public CommentAdapter(List<ReceiveQueryData> query, Context context) {
        this.query=query;
        this.context = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.comment, parent, false);
        return (new CommentViewHolder(view));
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Object name = query.get(position).getUId();
        String comment = query.get(position).getQMessage();
        String date = query.get(position).getQDate();
//        String image = null;
//        try {
//            image = notification.get(position).getFileName().get(0);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        holder.user_name.setText(name.toString());
        holder.queryComment.setText(comment);
        holder.queryDate.setText(date);
//        if (image != null) {
//            holder.notificationImage.setVisibility(View.VISIBLE);
//            Glide.with(context)
//                    .load(API.baseURL + "/file/notificationfile?faculty=bsccsit&name=" + image)
//                    .into(holder.notificationImage);
//        } else {
//            holder.notificationImage.setVisibility(View.GONE);
//            holder.notificationImage.setImageDrawable(null);
//        }
    }

    @Override
    public int getItemCount() {
        return query.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView queryComment;
        TextView user_name;
        TextView queryDate;

        public CommentViewHolder(View itemView) {
            super(itemView);
            queryComment = itemView.findViewById(R.id.tvCommentBody);
            user_name = itemView.findViewById(R.id.tv_name);
            queryDate = itemView.findViewById(R.id.tv_date);
        }
    }

    public void AddNotifications(List<ReceiveQueryData> newQuery) {
        for (ReceiveQueryData n : newQuery) {
            query.add(n);
        }
        notifyDataSetChanged();
    }
}
