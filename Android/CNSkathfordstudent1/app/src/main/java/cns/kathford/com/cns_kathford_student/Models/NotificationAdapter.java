package cns.kathford.com.cns_kathford_student.Models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cns.kathford.com.cns_kathford_student.R;

/**
 * Created by vikru on 7/9/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private List<ReceiveNotificationData> notification;
    private Context context;

    public NotificationAdapter(List<ReceiveNotificationData> notification, Context context) {
        this.notification = notification;
        this.context = context;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.notification_list, parent, false);
        return (new NotificationViewHolder(view));
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        String title = notification.get(position).getNTitle();
        String message = notification.get(position).getNMessage();
        String date = notification.get(position).getNDate();
//        String image = null;
//        try {
//            image = notification.get(position).getFileName().get(0);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        holder.notificationTitle.setText(title);
        holder.notificationMessage.setText(message);
        holder.notificationDate.setText(date);
//        if (image != null) {
//            holder.notificationImage.setVisibility(View.VISIBLE);
//            Glide.with(context)
//                    .load(API.baseURL + "/file/notificationfile?faculty=bsccsit&name=" + image)
//                    .into(holder.notificationImage);
//        } else {
//            holder.notificationImage.setVisibility(View.GONE);
//            holder.notificationImage.setImageDrawable(null);
//        }
    }

    @Override
    public int getItemCount() {
        return notification.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        ImageView notificationImage;
        TextView notificationMessage;
        TextView notificationTitle;
        TextView notificationDate;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            notificationMessage = itemView.findViewById(R.id.receive_n_message);
            notificationTitle = itemView.findViewById(R.id.receive_n_title);
            notificationDate = itemView.findViewById(R.id.receive_n_date_time);
            notificationImage = itemView.findViewById(R.id.receive_n_image);
        }
    }

    public void AddNotifications(List<ReceiveNotificationData> newNotification) {
        for (ReceiveNotificationData n : newNotification) {
            notification.add(n);
        }
        notifyDataSetChanged();
    }
}
