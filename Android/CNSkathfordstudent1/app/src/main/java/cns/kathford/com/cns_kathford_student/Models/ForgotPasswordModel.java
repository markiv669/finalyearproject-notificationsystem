package cns.kathford.com.cns_kathford_student.Models;

import java.util.HashMap;

import okhttp3.RequestBody;

public class ForgotPasswordModel {
    private String u_id;
    private String email;

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public HashMap<String, RequestBody> GetMap() {
        RequestBody u_id = RequestBody.create(okhttp3.MultipartBody.FORM, getU_id());
        RequestBody email = RequestBody.create(okhttp3.MultipartBody.FORM, getEmail());
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("u_id", u_id);
        map.put("email", email);
        return map;
    }
}
