package cns.kathford.com.cns_kathford_student;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Toast;

import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.FcmToken;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import me.pushy.sdk.Pushy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static cns.kathford.com.cns_kathford_student.LoginActivity.userSignupData;


public class Navigation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager fragmentManager = getSupportFragmentManager();
    Fragment fragment = null;
    private static String registrationStatus;

    protected void onStop() {
        Pushy.listen(this);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Pushy.listen(this);
        try {
            new Navigation.RegisterForPushNotificationsAsync().execute();
        } catch (Exception ex) {
            Log.d("\n\n\nTOKEN FOR PUSHY=", ex.getMessage() + " \n\n\n\n\n");
        }


        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setTitle("Home");
        fragmentManager.beginTransaction().add(R.id.fragment_container, new HomeFragment(), "HomeFragment").commit();
    }

    @Override
    public void onBackPressed() {
        fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fragment != null && !fragment.equals(fragmentManager.findFragmentByTag("HomeFragment"))) {
            setTitle("Home");
            fragmentManager.beginTransaction().remove(fragment).commit();
            fragmentManager.beginTransaction().add(R.id.fragment_container, new HomeFragment(), "HomeFragment").commit();

        } else {
            this.moveTaskToBack(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("ClickableViewAccessibility")
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id != R.id.nav_logout) {
            Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
            if (fragment != null) {
                fragmentManager.beginTransaction().remove(fragment).commit();
            }
        }
        fragment = null;
        switch (id) {
            case R.id.nav_home:
                setTitle("Home");
                fragment = fragmentManager.findFragmentById(R.id.fragment_container);
                if (fragment != null) {
                    fragmentManager.beginTransaction().remove(fragment).commit();
                }
                fragmentManager.beginTransaction().add(R.id.fragment_container, new HomeFragment(), "HomeFragment").commit();
                break;

            case R.id.nav_viewprofile:
                setTitle("Profile");
                fragment = new ViewProfileFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
                break;

            case R.id.nav_changepassword:
                setTitle("Change Password");
                fragment = new ChangePasswordFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
                break;

            case R.id.nav_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(Navigation.this);
                builder.setTitle("Logout");
                builder.setIcon(R.drawable.kath);
//                builder.setIcon(R.drawable.logout);
                builder.setMessage("Are you sure to Logout?").setCancelable(true)
                        .setPositiveButton("LOGOUT", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ClearLoginData();
                                Intent intent = new Intent(Navigation.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("Exit", true);
                                startActivity(intent);
                                finish();
                            }

                        })
                        .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                Button cancelBtn = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                cancelBtn.setTextColor(Color.BLACK);
                cancelBtn.setFocusable(true);
                Button okBtn = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                okBtn.setTextColor(Color.BLUE);
                break;

            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                CNSpreference prefs = new CNSpreference(getApplicationContext());
                // Assign a unique token to this device

                String deviceToken = Pushy.register(getApplicationContext());
                SendToken(deviceToken);


                String[] topics = new String[]{"bsccsit1", "bsccsit2", "bsccsit3", "bsccsit4", "bsccsit5", "bsccsit6", "bsccsit7", "bsccsit8"};
                Pushy.unsubscribe(topics, getApplicationContext());
                // String status = RegistarToken.SendToken(deviceToken); //send token to server
                String topic = (prefs.getUserFaculty() + prefs.getUserSemester()).toLowerCase();

                Pushy.subscribe(topic, getApplicationContext());
                Pushy.subscribe(prefs.getUserFaculty().toLowerCase() + "0", getApplicationContext());

            } catch (Exception exc) {
                //Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                return;
            }

            // Succeeded, do something to alert the user
        }
    }

    public String SendToken(String my_token) {
        //send this token to database
        if (my_token != null) {
            FcmToken token = new FcmToken();
            token.setTokenID(my_token);
//            token.setU_id(LoginActivity.userSignupData.getU_id());
            CNSpreference prefs = new CNSpreference(this);
            String data = prefs.getUserId();
            token.setU_id(data);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.baseURL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            API api = retrofit.create(API.class);

            Call<StatusModel> call = api.SendToken(token);


            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                    //In this point we got our hero list
                    //thats damn easy right ;)
                    StatusModel status = response.body();
                    if (status.getCode() == 0) {
                        registrationStatus = "failed";
                    } else {
                        registrationStatus = "sucess";
                    }

                    //now we can do whatever we want with this list
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    registrationStatus = "failed to reister to server";
                }
            });
        }
        return registrationStatus;
    }

    private class UnsubscribePushyAsync extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device
                String[] topics = new String[]
                        {"bsccsit0", "bsccsit1", "bsccsit2", "bsccsit3", "bsccsit4", "bsccsit5", "bsccsit6", "bsccsit7", "bsccsit8",
                                "bba0", "bba1", "bba2", "bba3", "bba4", "bba5", "bba6", "bba7", "bba8"};
                Pushy.unsubscribe(topics, getApplicationContext());
//                Pushy.unregister(getApplicationContext());
            } catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(getApplicationContext(), exc.toString(), Toast.LENGTH_LONG).show();
                return;
            }

            // Succeeded, do something to alert the user
        }
    }

    private void ClearLoginData() {
        CNSpreference mPrefs = new CNSpreference(this);
        mPrefs.clearData();
        try {
            new Navigation.UnsubscribePushyAsync().execute();
        } catch (Exception ex) {
            Log.d("PUshy:", ex.getMessage());
        }

    }
}
