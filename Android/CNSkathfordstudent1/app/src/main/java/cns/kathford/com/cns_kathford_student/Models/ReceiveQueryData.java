
package cns.kathford.com.cns_kathford_student.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiveQueryData {

    @SerializedName("n_id")
    @Expose
    private String nId;
    @SerializedName("u_id")
    @Expose
    private Object uId;
    @SerializedName("faculty")
    @Expose
    private Object faculty;
    @SerializedName("q_message")
    @Expose
    private String qMessage;
    @SerializedName("q_date")
    @Expose
    private String qDate;
    @SerializedName("q_time")
    @Expose
    private String qTime;

    public String getNId() {
        return nId;
    }

    public void setNId(String nId) {
        this.nId = nId;
    }

    public Object getUId() {
        return uId;
    }

    public void setUId(Object uId) {
        this.uId = uId;
    }

    public Object getFaculty() {
        return faculty;
    }

    public void setFaculty(Object faculty) {
        this.faculty = faculty;
    }

    public String getQMessage() {
        return qMessage;
    }

    public void setQMessage(String qMessage) {
        this.qMessage = qMessage;
    }

    public String getQDate() {
        return qDate;
    }

    public void setQDate(String qDate) {
        this.qDate = qDate;
    }

    public String getQTime() {
        return qTime;
    }

    public void setQTime(String qTime) {
        this.qTime = qTime;
    }

}
