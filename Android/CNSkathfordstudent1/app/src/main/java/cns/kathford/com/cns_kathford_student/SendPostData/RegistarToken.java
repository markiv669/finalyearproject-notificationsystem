package cns.kathford.com.cns_kathford_student.SendPostData;

import android.util.Log;
import android.widget.Toast;

import cns.kathford.com.cns_kathford_student.CNSpreference;
import cns.kathford.com.cns_kathford_student.LoginActivity;
import cns.kathford.com.cns_kathford_student.Models.API;
import cns.kathford.com.cns_kathford_student.Models.FcmToken;
import cns.kathford.com.cns_kathford_student.Models.StatusModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vikru on 6/8/2018.
 */

public class RegistarToken {

    private static String registrationStatus;

    public static String SendToken(String my_token) {
        //send this token to database
        if (my_token != null) {

            FcmToken token = new FcmToken();
            token.setTokenID(my_token);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.baseURL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            API api = retrofit.create(API.class);

            Call<StatusModel> call = api.SendToken(token);


            call.enqueue(new Callback<StatusModel>() {
                @Override
                public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {

                    //In this point we got our hero list
                    //thats damn easy right ;)
                    StatusModel status = response.body();
                    if (status.getCode() == 0) {
                        registrationStatus = "failed";
                    } else {
                        registrationStatus = "sucess";
                    }

                    //now we can do whatever we want with this list
                }

                @Override
                public void onFailure(Call<StatusModel> call, Throwable t) {
                    registrationStatus = "failed to reister to server";
                }
            });
        }
        return registrationStatus;
    }
}
