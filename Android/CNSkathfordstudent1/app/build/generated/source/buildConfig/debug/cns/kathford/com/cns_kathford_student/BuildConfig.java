/**
 * Automatically generated file. DO NOT MODIFY
 */
package cns.kathford.com.cns_kathford_student;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "cns.kathford.com.cns_kathford_student";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
